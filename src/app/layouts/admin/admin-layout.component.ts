import {Component, OnInit, ViewChild, ViewEncapsulation, ElementRef, AfterViewInit} from '@angular/core';
import 'rxjs/add/operator/filter';
import {state, style, transition, animate, trigger, AUTO_STYLE} from '@angular/animations';

import { MenuItems } from '../../shared/menu-items/menu-items';

import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

export interface Options {
  heading?: string;
  removeFooter?: boolean;
  mapHeader?: boolean;
}



@Component({
  selector: 'app-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.css'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('mobileMenuTop', [
        state('no-block, void',
            style({
                overflow: 'hidden',
                height: '0px',
            })
        ),
        state('yes-block',
            style({
                height: AUTO_STYLE,
            })
        ),
        transition('no-block <=> yes-block', [
            animate('400ms ease-in-out')
        ])
    ])
  ]
})

export class AdminLayoutComponent implements OnInit {
  deviceType = 'desktop';
  verticalNavType = 'expanded';
  verticalEffect = 'shrink';
  innerHeight: string;
  isCollapsedMobile = 'no-block';
  isCollapsedSideBar = 'no-block';
  toggleOn = true;
  windowWidth: number;

  public htmlButton: string;
  baseimg: string = localStorage.getItem('baseassets');
  baseimg2: string = localStorage.getItem('baseimg');

  username: string = '';
  role: string = '';
  photo: string = '';


  constructor(
    public menuItems: MenuItems,
    private http: HttpClient,
    public router: Router
    ) {

  }

  ngOnInit() {

    console.log('admin layout log');
    this.menuItems.reindex();
    this.username = localStorage.getItem('username');

    const formData = new FormData();

    formData.append('username', this.username);

    this.http.post(localStorage.getItem('baseurl') + 'getuser/', formData)
      .subscribe(
        res => {
          const rol = JSON.parse(JSON.parse(JSON.stringify(res)).role);
          this.role = rol[0].fields.name;
          localStorage.setItem('rol', JSON.stringify(rol[0].fields));
          this.photo = JSON.parse(JSON.stringify(res)).photo;
          console.log('foto ' + this.photo);
          if (this.photo === '') {
            this.photo = 'assets/images/user.png';
            this.baseimg2 = this. baseimg;
          }
        },
        error => {
          this.router.navigate(['/authentication/login']);
          console.log(error);
        }
      );
  }

  changeData() {
    this.router.navigate(['/perfil']);
  }

  onClickedOutside(e: Event) {
      if (this.windowWidth < 768 && this.toggleOn && this.verticalNavType !== 'offcanvas') {
          this.toggleOn = true;
          this.verticalNavType = 'offcanvas';
      }
  }

  onResize(event) {
    this.innerHeight = event.target.innerHeight + 'px';
    /* menu responsive */
    this.windowWidth = event.target.innerWidth;
    let reSizeFlag = true;
    if (this.deviceType === 'tablet' && this.windowWidth >= 768 && this.windowWidth <= 1024) {
      reSizeFlag = false;
    } else if (this.deviceType === 'mobile' && this.windowWidth < 768) {
      reSizeFlag = false;
    }

    if (reSizeFlag) {
      this.setMenuAttributs(this.windowWidth);
    }
  }

  setMenuAttributs(windowWidth) {
      if (windowWidth >= 768 && windowWidth <= 1024) {
        this.deviceType = 'tablet';
        this.verticalNavType = 'collapsed';
        this.verticalEffect = 'push';
      } else if (windowWidth < 768) {
        this.deviceType = 'mobile';
        this.verticalNavType = 'offcanvas';
        this.verticalEffect = 'overlay';
      } else {
        this.deviceType = 'desktop';
        this.verticalNavType = 'expanded';
        this.verticalEffect = 'shrink';
      }
  }

  toggleOpened() {
    if (this.windowWidth < 768) {
        this.toggleOn = this.verticalNavType === 'offcanvas' ? true : this.toggleOn;
        this.verticalNavType = this.verticalNavType === 'expanded' ? 'offcanvas' : 'expanded';
    } else {
        this.verticalNavType = this.verticalNavType === 'expanded' ? 'collapsed' : 'expanded';
    }
  }

  toggleOpenedSidebar() {
    this.isCollapsedSideBar = this.isCollapsedSideBar === 'yes-block' ? 'no-block' : 'yes-block';
  }

  onMobileMenu() {
    this.isCollapsedMobile = this.isCollapsedMobile === 'yes-block' ? 'no-block' : 'yes-block';
  }

  logout() {
    this.router.navigate(['/authentication/login']);
  }
}
