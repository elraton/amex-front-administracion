import { Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { ReporteCajaSoles } from '../common/reportecajasoles';
import { CajaSoles } from '../common/cajasoles';
import { IMyDpOptions } from 'mydatepicker';
import { MatSort, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.css']
})

export class ReportesComponent implements OnInit {
  select = 0;

  selectreport = '';
  provider = '';

  rol = JSON.parse(localStorage.getItem('rol'));
  showmov;
  showprov;
  showadel;
  showrendir;
  showpagar;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private http: HttpClient
  ) { }

  ngOnInit() {
    const formData = new FormData();
    formData.append('username', localStorage.getItem('username'));

    this.http.post(localStorage.getItem('baseurl') + 'getuser/', formData)
      .subscribe(
        res => {
          this.rol = JSON.parse(JSON.parse(JSON.stringify(res)).role)[0].fields;
          localStorage.setItem('rol', JSON.stringify(this.rol));
          this.showmov = this.rol.report_mov;
          this.showprov = this.rol.report_prov_balance;
          this.showadel = this.rol.report_prov_adv;
          this.showrendir = this.rol.report_to_back;
          this.showpagar = this.rol.report_to_pay;
        },
        error => {
          console.log(error);
        }
      );

    this.route.params.subscribe(params => {
      this.selectreport = params['type'];
      this.provider = params['provider'];
    });

    if (this.selectreport === '') {
      this.select = 0;
    }

    if (this.selectreport === 'caja') {
      this.select = 1;
    }

    if (this.selectreport === 'proveedor') {
      this.select = 2;
    }

    if (this.selectreport === 'adelantos') {
      this.select = 3;
    }

    if (this.selectreport === 'cuentas_rendir') {
      this.select = 4;
    }

    if (this.selectreport === 'cuentas_pagar') {
      this.select = 5;
    }

  }
  routeChange() {
    switch (+this.select) {
      case 0: {
        this.router.navigate(['/reportes']);
        break;
      }
      case 1: {
        this.router.navigate(['/reportes/caja']);
        break;
      }
      case 2: {
        this.router.navigate(['/reportes/proveedor']);
        break;
      }
      case 3: {
        this.router.navigate(['/reportes/adelantos']);
        break;
      }
      case 4: {
        this.router.navigate(['/reportes/cuentas_rendir']);
        break;
      }
      case 5: {
        this.router.navigate(['/reportes/cuentas_pagar']);
        break;
      }
      default: {
        break;
      }
    }
  }
}
