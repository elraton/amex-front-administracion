import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { Page } from '../../common/page';
import { ReportPorRendir } from '../../common/reportporrendir';
import {IMyDpOptions} from 'mydatepicker';
import {MatSort, MatTableDataSource} from '@angular/material';
import {RegistroAux} from '../../common/registroaux';
import {SelectionModel} from '@angular/cdk/collections';
import {Sort} from '@angular/material';


@Component({
  selector: 'app-report-porrendir',
  templateUrl: './porrendir.component.html',
  styleUrls: ['./porrendir.component.css']
})
export class PorRendirComponent implements OnInit {

  displayedColumns = [
    // 'select',
    'id',
    'provider',
    'amount_soles',
    'amount_dolares',
    'aux'
  ];

  dataSource;

  ELEMENT_DATA;

  baseimg2 = localStorage.getItem('baseassets');

  @ViewChild(MatSort) sort: MatSort;

  selection;

  isLoadingResults: boolean;

  proveedores_list: any[];
  categoria_list: any[];

  showNotFound: string;
  activeExport: string;

  disablePrev: string;
  disableNext: string;

  page: number;
  totalpages: number;
  filterSelect: number;
  paginator: any[];

  fecha: string;
  proveedores: string;
  categoria: string;

  proveedoresv: string;
  categoriav: string;

  isLoading = true;
  showData = false;

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'yyyy-mm-dd',
    dayLabels: {
      su: 'Dom',
      mo: 'Lun',
      tu: 'Mar',
      we: 'Mier',
      th: 'Jue',
      fr: 'Vie',
      sa: 'Sab'},
    monthLabels: {
      1: 'Enero',
      2: 'Febrero',
      3: 'Marzo',
      4: 'Abril',
      5: 'Mayo',
      6: 'Junio',
      7: 'Julio',
      8: 'Agosto',
      9: 'Setiembre',
      10: 'Octubre',
      11: 'Noviembre',
      12: 'Diciembre'
    },
    allowDeselectDate: true,
    todayBtnTxt: 'Hoy',
    sunHighlight: true,
    markCurrentDay: true,
    openSelectorOnInputClick: true,
    inline: false,
    editableDateField: false,
    selectorWidth: '400px'
  };

  public date1: any;
  public date2: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private http: HttpClient,
    private modalService: ModalDialogService,
    private viewRef: ViewContainerRef
    ) { }

  ngOnInit() {

    this.isLoadingResults = true;
    this.activeExport = 'disabled';

    this.http.get(localStorage.getItem('baseurl') + 'list_documents/add/')
      .subscribe(
        res => {
          let aux = JSON.stringify(res);
          this.proveedores_list = JSON.parse(aux).providers;
        },
        error => {
          console.log(error.error.text);
        }
      );


    this.categoria_list = JSON.parse(localStorage.getItem('categories'));

    this.disablePrev = 'disabled';
    this.disableNext = '';

    this.page = 1;
    this.totalpages = 1;
    this.filterSelect = 0;

    this.fecha = 'no-display';
    this.proveedores = 'no-display';
    this.categoria = 'no-display';


    this.route.params.subscribe(params => {
      this.page = +params['p'];
    });

    if(this.page == 1) {
      this.disablePrev = 'disabled';
      this.disableNext = 'disabled';
    }
    if(this.page == this.totalpages) {
      this.disableNext = 'disabled';
    }
    if(this.page > 1) {
      this.disablePrev = '';
    }
    if(this.page < this.totalpages) {
      this.disableNext = '';
    }


    this.http.get(localStorage.getItem('baseurl') + 'list_to_back/')
      .subscribe(
        res => {

          let aux = JSON.stringify(res);

          if (JSON.parse(aux).objects.length === 0) {
            this.isLoading = false;
            this.showData = false;
            return;
          }

          this.totalpages = JSON.parse(aux).num_pages;
          this.paginator = [];
          for (let i = 0; i < this.totalpages; i++) {
            if(i+1 == this.page) {
              this.paginator.push({page:i+1, state: 'active'});
            } else {
              this.paginator.push({page:i+1, state: ''});
            }
          }

          this.ELEMENT_DATA = JSON.parse(aux).objects;

          this.isLoadingResults = false;
          //this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
          this.dataSource = new MatTableDataSource<ReportPorRendir>(this.ELEMENT_DATA);
          this.selection = new SelectionModel<ReportPorRendir>(true, []);
          this.dataSource.sort = this.sort;

          this.isLoading = false;
          this.showData = true;


        },
        error => {
          console.log(localStorage.getItem('baseurl'));
          console.log(error.error.text);
          this.isLoading = false;
          this.showData = false;
        }
      );
  }

  stripDate(str: string) {
    return str.split('T')[0];
  }

  export() {
    if(this.selection.hasValue()){
      const data = [];
      for(let xx of this.selection.selected) {
        data.push({'id': +xx.aux});
      }

      this.http.post(localStorage.getItem('baseurl') + 'export_documents/', data)
        .subscribe(
          response => {
            const resaux = JSON.stringify(response);
            console.log(JSON.parse(resaux));
          },
          error => {
            console.log(error);
            if (error.error.text.indexOf('Viaje') !== -1){
              console.log(error.error.text);
              const blob = new Blob(['\ufeff' + error.error.text], { type: 'text/csv;charset=utf-8;' });
              const dwldLink = document.createElement('a');
              const url = URL.createObjectURL(blob);
              const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
              if (isSafariBrowser) {
                dwldLink.setAttribute('target', '_blank');
              }
              dwldLink.setAttribute('href', url);
              dwldLink.setAttribute('download', 'documentos.csv');
              dwldLink.style.visibility = 'hidden';
              document.body.appendChild(dwldLink);
              dwldLink.click();
              document.body.removeChild(dwldLink);
            }
          });


    }
  }

  selectChange(e: any, row: any) {
    e ? this.selection.toggle(row) : null;


    if(this.selection.hasValue()){
      this.activeExport = '';
    } else {
      this.activeExport = 'disabled';
    }
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  parseType(x) {
    let list_types = [
      {id: '1', name: 'Caja Soles'},
      {id: '2', name: 'Caja Dólares'},
      {id: '3', name: 'Banco M/N Cta Cte'},
      {id: '4', name: 'Banco M/E Cta Cte'},
    ];
    for(let xx of list_types) {
      if(xx.id == x) {
        return xx.name;
      }
    }
  }

  showDocuments(docs) {
    console.log(docs);
    let content = '';
    for(let xx of JSON.parse(JSON.stringify(docs))){
      content = content + '<div class="documentPopup" [ngStyle]="{\'border\':\'2px dotted\'},{\'padding\':\'15px\'},{\'margin-bottom\':\'15px\'}">';
      content = content + '<p><strong>Fecha: </strong>'+xx.date.split('T')[0]+'</p>';
      content = content + '<p><strong>Descripción: </strong>'+xx.description+'</p>';
      content = content + '<p><strong>Monto: </strong>'+xx.amount+'</p>';
      content = content + '<p><strong>Tipo de Documento: </strong>'+xx.document_type+'</p>';
      content = content + '<p><strong>Número de Documento: </strong>'+xx.document_number+'</p>';
      content = content + '</div>';
    }
    this.modalService.openDialog(this.viewRef, {
      title: 'Documentos',
      childComponent: SimpleModalComponent,
      data: {
        text: content
      },
      settings: {
        closeButtonClass: 'close theme-icon-close'
      },
      actionButtons: [
        {
          text: 'Cerrar',
          buttonClass: 'btn btn-success',
          onAction: () => new Promise((resolve: any) => {
            setTimeout(() => {
              resolve();
            }, 20);
          })
        }
      ]
    });
  }


  changePage(p) {
    this.isLoadingResults = true;
    for(let i of this.paginator) {
      i.state = '';
      if (i.page == p) {
        i.state = 'active';
      }
    }
    this.page = p;
    if(this.page == 1) {
      this.disablePrev = 'disabled';
      this.disableNext = 'disabled';
    }
    if(this.page == this.totalpages) {
      this.disableNext = 'disabled';
    }
    if(this.page > 1) {
      this.disablePrev = '';
    }
    if(this.page < this.totalpages) {
      this.disableNext = '';
    }

    this.location.replaceState("/registro/"+this.page);

    this.http.get(localStorage.getItem('baseurl') + 'list_outlays?page='+this.page)
      .subscribe(
        res => {
          let aux = JSON.stringify(res);

          this.ELEMENT_DATA = JSON.parse(aux).objects;

          this.isLoadingResults = false;
          //this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
          this.dataSource = new MatTableDataSource<ReportPorRendir>(this.ELEMENT_DATA);
          this.selection = new SelectionModel<ReportPorRendir>(true, []);
          this.dataSource.sort = this.sort;
        },
        error => {
          console.log(error.error.text);
        }
      );
  }

  editTrip(v) {
    this.router.navigate(['/reportes/proveedor/'+v]);
  }
  viewTrip(v) {
    let vv;
    for (let xx of this.ELEMENT_DATA) {
      if (xx.id.toString() === v) {
        vv = xx;
        localStorage.setItem("edittrip", JSON.stringify(vv));
        this.router.navigate(['/transferencias/view/'+v]);
        return;
      }
    }
  }

  sortData(sort: Sort) {
    if (sort.direction !== '') {
      this.isLoading = true;
      const data = this.ELEMENT_DATA.slice();
      if (!sort.active) {
        this.ELEMENT_DATA = data;
        return;
      }

      this.ELEMENT_DATA = data.sort((a, b) => {
        const isAsc = sort.direction === 'asc';
        switch (sort.active) {
          case 'provider': return compare(a.name, b.name, isAsc);
          case 'amount': return compare(a.amount, b.amount, isAsc);
          default: return 0;
        }
      });

      // this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
      this.dataSource = new MatTableDataSource<ReportPorRendir>(this.ELEMENT_DATA);
      this.selection = new SelectionModel<ReportPorRendir>(true, []);
      this.dataSource.sort = this.sort;
      this.isLoading = false;
    }
  }

  deleteTrip(v) {
    let vv;
    for (let xx of this.ELEMENT_DATA) {
      if (xx.id.toString() === v) {
        vv = xx;
        break;
      }
    }
    this.modalService.openDialog(this.viewRef, {
      title: '¿Seguro que quieres eliminar el Registro?',
      childComponent: SimpleModalComponent,
      data: {
        text: ''
      },
      settings: {
        closeButtonClass: 'close theme-icon-close'
      },
      actionButtons: [
        {
          text: 'Si',
          buttonClass: 'btn btn-danger',
          onAction: () => new Promise((resolve: any, reject: any) => {
            setTimeout(() => {
              const varData = new FormData();
              varData.append('id', v);
              this.http.post(localStorage.getItem('baseurl') + 'list_outlays/delete/', varData)
                .subscribe(
                  res => {
                    console.log(res);
                    //this.location.back();
                  },
                  error => {
                    console.log(error.error.text);
                    if(error.error.text === 'Register deleted.') {
                      resolve();
                      this.isLoadingResults = true;

                      this.ELEMENT_DATA = this.ELEMENT_DATA.filter(obj => obj !== vv);

                      this.isLoadingResults = false;
                      //this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
                      this.dataSource = new MatTableDataSource<ReportPorRendir>(this.ELEMENT_DATA);
                      this.selection = new SelectionModel<ReportPorRendir>(true, []);
                      this.dataSource.sort = this.sort;

                      this.isLoadingResults = true;
                    }
                  }
                );
            }, 20);
          })
        },
        {
          text: 'No',
          buttonClass: 'btn btn-success',
          onAction: () => new Promise((resolve: any) => {
            setTimeout(() => {
              resolve();
            }, 20);
          })
        },
      ]
    });

  }

  filtrar() {

  }

  previusPage() {
    this.changePage(this.page - 1);
  }

  nextPage() {
    this.changePage(this.page + 1);
  }

  filtrarForm(e, val) {
    switch (val) {
      case "0":{
        this.fecha = 'no-display';
        this.proveedores = 'no-display';
        this.categoria = 'no-display';
        break;
      }
      case "1":{
        this.fecha = '';
        this.proveedores = 'no-display';
        this.categoria = 'no-display';
        break;
      }
      case "2":{
        this.fecha = 'no-display';
        this.proveedores = '';
        this.categoria = 'no-display';
        break;
      }
      case "3":{
        this.fecha = 'no-display';
        this.proveedores = 'no-display';
        this.categoria = '';
        break;
      }
      default:
        // code...
        break;
    }
  }

}

function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
