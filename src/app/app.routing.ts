import {Routes} from '@angular/router';

import {AdminLayoutComponent} from './layouts/admin/admin-layout.component';
import {AuthLayoutComponent} from './layouts/auth/auth-layout.component';

import { AuthGuard } from './guard';

export const AppRoutes: Routes = [{
  path: '',
  component: AdminLayoutComponent,
  children: [
    {
      path: '',
      canActivateChild: [AuthGuard],
      redirectTo: 'registro/1',
      pathMatch: 'full'
    },
    {
      path: 'viajes',
      canActivateChild: [AuthGuard],
      redirectTo: 'viajes/1'
    },
    {
      path: 'viajes',
      canActivateChild: [AuthGuard],
      loadChildren: './viajes/viajes.module#ViajesModule'
    },
    {
      path: 'registro',
      canActivateChild: [AuthGuard],
      redirectTo: 'registro/1'
    },
    {
      path: 'registro',
      canActivateChild: [AuthGuard],
      loadChildren: './registro/registro.module#RegistroModule'
    },
    {
      path: 'dinero',
      canActivateChild: [AuthGuard],
      redirectTo: 'dinero/1'
    },
    {
      path: 'dinero',
      canActivateChild: [AuthGuard],
      loadChildren: './salidadinero/salidadinero.module#SalidaDineroModule'
    },
    {
      path: 'proveedores',
      canActivateChild: [AuthGuard],
      redirectTo: 'proveedores/1'
    },
    {
      path: 'proveedores',
      canActivateChild: [AuthGuard],
      loadChildren: './proveedores/proveedores.module#ProveedoresModule'
    },
    {
      path: 'utilidades',
      canActivateChild: [AuthGuard],
      loadChildren: './utilidades/utilidades.module#UtilidadesModule'
    },
    {
      path: 'cierrecaja',
      canActivateChild: [AuthGuard],
      loadChildren: './cierrecaja/cierrecaja.module#CierreCajaModule'
    },
    {
      path: 'reportes',
      canActivateChild: [AuthGuard],
      loadChildren: './reportes/reportes.module#ReportesModule'
    },
    {
      path: 'perfil',
      canActivateChild: [AuthGuard],
      loadChildren: './perfil/perfil.module#PerfilModule'
    }

  ]
}, {
  path: '',
  component: AuthLayoutComponent,
  children: [
    {
      path: 'authentication',
      loadChildren: './authentication/authentication.module#AuthenticationModule'
    }
  ]
}, {
  path: '**',
  redirectTo: 'error/404'
}];
