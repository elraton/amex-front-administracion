import { Routes } from '@angular/router';

import { SalidaDineroComponent } from './salidadinero.component';
import { ShowSalidaDineroComponent } from './showsalidadinero/showsalidadinero.component';
import { AddSalidaDineroComponent } from './addsalidadinero/addsalidadinero.component';
import { EditSalidaDineroComponent } from './editsalidadinero/editsalidadinero.component';
import { ViewSalidaDineroComponent } from './viewsalidadinero/viewsalidadinero.component';
import { FilterSalidaDineroComponent } from './filtersalidadinero/filtersalidadinero.component';
import { AddDevolucionesComponent } from './adddevoluciones/adddevoluciones.component';
import { AddTransferenciasComponent } from './addtransferencias/addtransferencias.component';
import { AddIngresosComponent } from './addingresos/addingresos.component';
import { CanDeactivateGuard } from '../preventexit/prevent';

export const SalidaDineroRoutes: Routes = [{
  path: '',
  component: SalidaDineroComponent,
  data: {
    breadcrumb: 'Registro de movimientos',
    status: true
  },
  children: [
      {
        path: ':p',
          component: ShowSalidaDineroComponent,
          data: {
              breadcrumb: 'Registro de movimientos',
              status: false
          }
      },
      {
        path: 'add/new',
          component: AddSalidaDineroComponent,
          data: {
              breadcrumb: 'Añadir Desembolso',
              status: false
          },
          canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'add/devoluciones',
          component: AddDevolucionesComponent,
          data: {
              breadcrumb: 'Añadir Devolución',
              status: false
          },
          canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'add/transferencias',
          component: AddTransferenciasComponent,
          data: {
              breadcrumb: 'Añadir Transferencia',
              status: false
          },
          canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'add/ingresos',
          component: AddIngresosComponent,
          data: {
              breadcrumb: 'Añadir Ingresos',
              status: false
          },
          canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'edit/:id',
        component: EditSalidaDineroComponent,
        data: {
          breadcrumb: 'Editar Registro de movimientos',
          status: false
        },
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'view/:id',
        component: ViewSalidaDineroComponent,
        data: {
          breadcrumb: 'Ver Registro de movimientos',
          status: false
        }
      },
      {
        path: 'filter/:type/:par1/:par2/:page',
        component: FilterSalidaDineroComponent,
        data: {
          breadcrumb: 'Filtrar Registro de movimientos',
          status: false
        }
      },
      {
        path: 'filter/:type/:par1/:page',
        component: FilterSalidaDineroComponent,
        data: {
          breadcrumb: 'Filtrar Registro de movimientos',
          status: false
        }
      }
    ]
}];
