import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PerfilComponent } from './perfil.component';
import { PerfilRoutes } from './perfil.routing';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PerfilRoutes),
      SharedModule
  ],
  declarations: [PerfilComponent]
})

export class PerfilModule {}
