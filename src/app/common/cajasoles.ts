export interface CajaSoles {
	id: number;
	description: string;
	exchange_rate: string;
	date: string;
	cheque_number: string;
	category: string;
	currency: string;
	trip: string;
	amount: string;
	provider: string;
	operation_type: string;
	type: string;
}