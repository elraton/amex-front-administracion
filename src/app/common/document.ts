export interface Document {
    id: number;
    trip: string;
    date: string;
    category: string;
    provider: string;
    provider_origin: string;
    send: string;
    send_date: string;
    document_type: string;
    document_number: string;
    document_photo: string;
    description: string;
    amount: string;
    agree: string;
    currency: string;
    exchange_rate: string;
}
