export interface ReportPorRendir {
    id: number;
    name: string;
    amount: string;
}