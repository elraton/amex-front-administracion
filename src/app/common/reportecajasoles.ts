export interface ReporteCajaSoles {
	nmo: number;
	fecha: string;
	proveedor: string;
	doc: any[];
	tipoope: string;
	detalle: string;
	debe: string;
	haber: string;
	saldo: string;
	viaje: string;
}
