export interface CashClosed {
	rest: string;
	type: string;
	year: number;
}
