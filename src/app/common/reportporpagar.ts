export interface ReportPorPagar {
    id: number;
    name: string;
    amount: string;
}
