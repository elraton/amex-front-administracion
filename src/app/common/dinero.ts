export interface Dinero {
id: number;
trip: string;
date: string;
operation_type: number;
category: number;
provider: string;
description: string;
cheque_number: string;
exchange_rate: string;
type: string;
amount: string;
currency: string;
}
