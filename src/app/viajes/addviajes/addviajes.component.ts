import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';

import { Trip } from '../../common/trip';
import { Budget } from '../../common/budget';
import {IMyDpOptions} from 'mydatepicker';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';


@Component({
  selector: 'app-add-viajes',
  templateUrl: './addviajes.component.html',
  styleUrls: ['./addviajes.component.css']
})
export class AddViajesComponent implements OnInit {

  Viajes: any;
  Budgets: Budget[];

  myControl2: FormControl = new FormControl();
  options2 = [];

  filteredOptions2: Observable<string[]>;

  list_categories: any[];
  list_categories_copy: any[];

  baseimg2: string = localStorage.getItem('baseassets');

  id: number;

  list_types: any[];
  trip: Trip;

  isSaved: boolean;

  list_providers: any[];

  isloading: boolean;


  code: string;
  description: string;
  date: string;
  type: string;
  date_end: string;
  rate: number;
  operations: boolean;
  management: boolean;

  trip_status: number;
  trip_operations: number;
  trip_management: number;

  showSpin: string;

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'dd/mm/yyyy',
    dayLabels: {
      su: 'Dom',
      mo: 'Lun',
      tu: 'Mar',
      we: 'Mier',
      th: 'Jue',
      fr: 'Vie',
      sa: 'Sab'},
    monthLabels: {
      1: 'Enero',
      2: 'Febrero',
      3: 'Marzo',
      4: 'Abril',
      5: 'Mayo',
      6: 'Junio',
      7: 'Julio',
      8: 'Agosto',
      9: 'Setiembre',
      10: 'Octubre',
      11: 'Noviembre',
      12: 'Diciembre'
    },
    allowDeselectDate: true,
    todayBtnTxt: 'Hoy',
    sunHighlight: true,
    markCurrentDay: true,
    openSelectorOnInputClick: true,
    inline: false,
    editableDateField: true,
    selectorWidth: '400px',
  };

  public date1: any;
  public date2: any;

  rol = JSON.parse(localStorage.getItem('rol'));
  showbudget = this.rol.trip_budget;

  constructor(
    private router: Router,
    private location: Location,
    private route: ActivatedRoute,
    private http: HttpClient,
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig
    ) { }

  ngOnInit() {

    this.isloading = true;

    this.Budgets = [];
    this.showSpin = 'hideLoading';
    this.isSaved = false;
    this.list_types = JSON.parse(localStorage.getItem('trip_types'));
    this.code = '';
    this.description = '';
    this.date = '';
    this.type = '';
    this.date_end = '';
    this.rate = 0;
    this.operations = false;
    this.management = false;

    this.list_categories = JSON.parse(localStorage.getItem('categories'));
    this.list_categories_copy = [];

    this.http.get(localStorage.getItem('baseurl') + 'list_documents/add/')
      .subscribe(
        res => {
          let aux = JSON.stringify(res);
          this.list_providers = JSON.parse(aux).providers;

          for(let xx of this.list_providers) {
            this.options2.push(xx.name);
          }

        this.filteredOptions2 = this.myControl2.valueChanges
          .pipe(
            startWith(''),
            map(val => this.filter2(val))
          );

          this.isloading = false;

        },
        error => {
          console.log(error.error.text);
        }
      );

    for (let xx of this.list_categories) {
      this.list_categories_copy.push(
        {
          name: xx.name,
          id: xx.id,
          disable: false
        }
      );
    }

    this.trip_status = 1;
    this.trip_management = 1;
    this.trip_operations = 1;

    this.type = '0';

    this.trip = JSON.parse('{"code": "' + this.code + '"}');
    this.trip.code = this.code;
  }

  filterCategories(b) {
    for (let xx of this.list_categories_copy) {
      if (+b === xx.id) {
        xx.disable = true;
      }
    }
  }

  canDeactivate() {
    console.log('i am navigating away');
    if(this.isSaved == false){
      return window.confirm('¿Realmente Desea Salir?');
    }
    return true;
  }

  filter2(val: string): string[] {
    return this.options2.filter(option =>
      option.toLowerCase().includes(val.toLowerCase()));
  }

  starsClick(x) {
    this.rate = x;
  }

  montoChange(b) {
    b.amount = Number(b.amount).toFixed(2);
  }

  numberChange(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    const inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode === 46) {

    } else {
      if (event.keyCode === 32 || event.keyCode === 8 || !pattern.test(inputChar)) {
        event.preventDefault();
      }
    }

  }

  toggleCurrency(b) {
    if (b.currency === 'S') {
      b.currency = 'D';
    } else {
      b.currency = 'S';
    }
  }

  addBudget() {
    this.Budgets.push(
      {
        id: 0,
        trip: '',
        category: '0',
        amount: '',
        currency: 'D'
      }
    );
  }

  deleteBudget(b) {
    this.Budgets = this.Budgets.filter(obj => obj !== b);
  }

  text2upper(event: any) {
    const inputChar = String.fromCharCode(event.charCode);
    this.code = this.code.toUpperCase();
  }

  cleanForm() {
    this.Budgets = [];
    this.showSpin = 'hideLoading';
    this.isSaved = false;
    this.code = '';
    this.description = '';
    this.date = '';
    this.date_end = '';
    this.rate = 0;
    this.type = '0';
    this.operations = false;
    this.management = false;
    // this.date1 = null;
  }

  save(opt: Number) {
    this.trip.code = this.code;
    this.trip.description = this.description;

    this.trip.type = this.type;

    if (this.date1 !== undefined && this.date1 !== null) {
      if (this.date1.formatted !== undefined) {
        this.trip.date = this.date1.formatted.split('/')[2] + '-' + this.date1.formatted.split('/')[1] + '-' + this.date1.formatted.split('/')[0];
      } else {
        this.trip.date = this.date;
      }
    } else {
      this.trip.date = this.date;
    }

    this.trip.date_end = '';

    this.trip.rate = this.rate.toString();

    if(this.trip_operations === 1) {
      this.trip.operations = false;
    } else {
      this.trip.operations = true;
    }

    if(this.trip_management === 1) {
      this.trip.management = false;
    } else {
      this.trip.management = true;
    }

    this.trip.agency = '';
    /*if (this.myControl2.value !== null || this.myControl2.value !== '') {
      for (let xx of this.list_providers) {
        if (xx.name === this.myControl2.value) {
          this.trip.agency = xx.id;
        }
      }
    }*/

    let error: boolean = false;

    /*if (this.trip.agency === '') {
      this.toastyService.warning('No existe la agencia');
      error = true;
    }*/

    if(this.trip.code.length < 3) {
      this.toastyService.warning('Ingrese un código');
      error = true;
    }

    if(this.trip.date == '') {
      this.toastyService.warning('Ingrese una Fecha de Inicio de Viaje');
      error = true;
    }

    if(this.trip.type == '0') {
      this.toastyService.warning('Seleccione un Tipo de Viaje');
      error = true;
    }

    if (error === false) {
      const formData = new FormData();
      formData.append('id', '');
      formData.append('code', this.trip.code);
      formData.append('description', this.trip.description);
      formData.append('date', this.trip.date);
      formData.append('type', this.trip.type);
      formData.append('date_end', this.trip.date_end);
      formData.append('rate', this.trip.rate);
      formData.append('operations', this.trip.operations ? '1' : '0');
      formData.append('management', this.trip.management ? '1' : '0');
      formData.append('agency', this.trip.agency);

      this.http.post(localStorage.getItem('baseurl') + 'list_trips/add/', formData)
      .subscribe(
        res => {
          this.isSaved = true;
          this.toastyService.success('Viaje creado');
          const aux = JSON.parse(JSON.stringify(res));
          this.Budgets = this.Budgets.filter(
            (obj) => {
              if (obj.amount !== '') {
                obj.trip = aux.id;
                return obj;
              }
          });

          if (this.Budgets.length > 0) {
            const data = new FormData();
            data.append('budget', JSON.stringify(this.Budgets));

            this.http.post(localStorage.getItem('baseurl') + 'list_budgets/add/', data)
                .subscribe(
                  res2 => {
                    this.toastyService.success('Presupuesto guardado');
                      this.isSaved = true;
                      if (opt === 1) {
                        setTimeout(() => {
                          this.location.back();
                        }, 1000);
                      } else {
                        this.cleanForm();
                      }
                  },
                  error1 => {
                    console.log(error1.error.text);
                    this.toastyService.warning('Ocurrio un error al guardar los Presupuestos');
                    this.isSaved = true;
                    if (opt === 1) {
                      setTimeout(() => {
                        this.location.back();
                      }, 1000);
                    } else {
                      this.cleanForm();
                    }
                  }
                );
          } else {
            this.isSaved = true;
            if (opt === 1) {
              setTimeout(() => {
                this.location.back();
              }, 2000);
            } else {
              this.cleanForm();
            }
          }
        },
        error => {
          console.log(error.error.text);
          this.toastyService.error('No se pudo guardar el viaje');
        }
      );

    }


  }

  goback() {
    this.location.back();
  }

  cancelar() {
    this.location.back();
  }

}
