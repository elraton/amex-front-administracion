import { Routes } from '@angular/router';

import { ViajesComponent } from './viajes.component';
import { PlanillasComponent } from './planillas/planillas.component';
import { ResumenComponent } from './resumen/resumen.component';
import { ResumenDetalleComponent } from './resumendetalle/resumendetalle.component';
import { ViajesBComponent } from './viajes/viajesb.component';
import { EditViajesComponent } from './editviajes/editviajes.component';
import { AddViajesComponent } from './addviajes/addviajes.component';
import { ViewViajesComponent } from './viewviajes/viewviajes.component';
import { FilterViajesComponent } from './filter/filterviajes.component';
import { CanDeactivateGuard } from '../preventexit/prevent';

export const ViajesRoutes: Routes = [{
  path: '',
  component: ViajesComponent,
  data: {
    breadcrumb: 'Viajes',
    status: true
  },
  children: [
      {
        path: ':p',
          component: ViajesBComponent,
          data: {
              breadcrumb: 'Viajes',
              status: false
          }
      },
      {
        path: 'planillas/:id/:p',
          component: PlanillasComponent,
          data: {
              breadcrumb: 'Planillas',
              status: false
          }
      },
      {
        path: 'edit/:id',
          component: EditViajesComponent,
          data: {
              breadcrumb: 'Editar Viaje',
              status: false
          },
          canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'add/new',
          component: AddViajesComponent,
          data: {
              breadcrumb: 'Añadir Viaje',
              status: false
          },
          canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'view/:id',
          component: ViewViajesComponent,
          data: {
              breadcrumb: 'Ver Viaje',
              status: false
          }
      },
      {
        path: 'filter/:t/:x/:y/:z',
          component: FilterViajesComponent,
          data: {
              breadcrumb: 'Filtros de Viaje',
              status: false
          }
      },
      {
        path: 'filter/:t/:x/:y',
        component: FilterViajesComponent,
        data: {
          breadcrumb: 'Filtros de Viaje',
          status: false
        }
      },
      {
        path: 'resumen/:id',
        component: ResumenComponent,
        data: {
          breadcrumb: 'Resumen de Viaje',
          status: false
        }
      },
      {
        path: 'resumen/detalle/:trip/:cat/:p',
        component: ResumenDetalleComponent,
        data: {
          breadcrumb: 'Resumen de Viaje - Detalle',
          status: false
        }
      }

    ]
}];
