import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { Page } from '../../common/page';
import { Trip } from '../../common/trip';
import {IMyDpOptions} from 'mydatepicker';
import {SelectionModel} from '@angular/cdk/collections';
import {ViajesAux} from '../../common/viajeaux';
import {MatSort, MatTableDataSource} from '@angular/material';


@Component({
  selector: 'app-viajes-viajes',
  templateUrl: './filterviajes.component.html',
  styleUrls: ['./filterviajes.component.css']
})
export class FilterViajesComponent implements OnInit {

  displayedColumns = [
    'select',
    'resumen',
    'codigo',
    'fecha',
    'fechafin',
    'operaciones',
    'gerencia',
    'tipo',
    'calificacion',
    'aux'
  ];
  dataSource;

  baseimg2: string = localStorage.getItem('baseassets');

  ELEMENT_DATA: ViajesAux[];

  @ViewChild(MatSort) sort: MatSort;

  selection;
  isLoadingResults: boolean;

  fecha:string;
	fechaFin:string;
	operaciones:string;
	gerencia:string;
	tipoViaje:string;
  estado: string;

  activeExport: string;

  Viajes: Trip[];
  page: number;
  totalpages: number;
  paginator: Page[];

  disablePrev: string;
  disableNext: string;

  showLoading: string;
  showTableData: string;

  filterSelect: number;


  dates1: string;
  datee1: string;

  dates2: string;
  datee2: string;

  estadov: string;
  operacionesv: string;
  gerenciav: string;

  list_types: any[];
  tripType: string;

  code2search:string;

  showNotFound: string;
  isLoading = true;
  showData = false;

  fechasFilter = 0;

  type: string;
  x: string;
  y: string;

  exportMessage = 'Exportar Todo';

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'dd/mm/yyyy',
    dayLabels: {
      su: 'Dom',
      mo: 'Lun',
      tu: 'Mar',
      we: 'Mier',
      th: 'Jue',
      fr: 'Vie',
      sa: 'Sab'},
    monthLabels: {
      1: 'Enero',
      2: 'Febrero',
      3: 'Marzo',
      4: 'Abril',
      5: 'Mayo',
      6: 'Junio',
      7: 'Julio',
      8: 'Agosto',
      9: 'Setiembre',
      10: 'Octubre',
      11: 'Noviembre',
      12: 'Diciembre'
    },
    allowDeselectDate: true,
    todayBtnTxt: 'Hoy',
    sunHighlight: true,
    markCurrentDay: true,
    openSelectorOnInputClick: true,
    inline: false,
    editableDateField: true,
    selectorWidth: '400px'
  };

  public myDatePickerOptions2: IMyDpOptions = {
    // other options...
    dateFormat: 'dd/mm/yyyy',
    dayLabels: {
      su: 'Dom',
      mo: 'Lun',
      tu: 'Mar',
      we: 'Mier',
      th: 'Jue',
      fr: 'Vie',
      sa: 'Sab'},
    monthLabels: {
      1: 'Enero',
      2: 'Febrero',
      3: 'Marzo',
      4: 'Abril',
      5: 'Mayo',
      6: 'Junio',
      7: 'Julio',
      8: 'Agosto',
      9: 'Setiembre',
      10: 'Octubre',
      11: 'Noviembre',
      12: 'Diciembre'
    },
    allowDeselectDate: true,
    todayBtnTxt: 'Hoy',
    sunHighlight: true,
    markCurrentDay: true,
    openSelectorOnInputClick: true,
    inline: false,
    editableDateField: true,
    selectorWidth: '400px'
  };

  public date1: any;
  public date2: any;
  public date3: any;
  public date4: any;

  rol = JSON.parse(localStorage.getItem('rol'));
  showadd = this.rol.trip_add;
  showview = this.rol.trip_view;
  showedit = this.rol.trip_edit;
  showdelete = this.rol.trip_delete;
  showexport = this.rol.trip_export;
  showabstract = this.rol.trip_abstract;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private http: HttpClient,
    private modalService: ModalDialogService,
    private viewRef: ViewContainerRef
    ) { }

  ngOnInit() {
    if (!this.showabstract) {
      this.displayedColumns = this.displayedColumns.filter(nam => nam !== 'resumen');
    }
    this.activeExport = 'disabled';
    this.fecha = 'no-display';
    this.fechaFin = 'no-display';
    this.operaciones = 'no-display';
    this.gerencia = 'no-display';
    this.tipoViaje = 'no-display';
    this.estado = 'no-display';

    this.isLoadingResults = true;

    this.showLoading = 'show-loading';
    this.showTableData = 'hide-data';
    this.showNotFound = 'hide-loading';

    this.disablePrev = 'disabled';
    this.disableNext = '';

    this.page = 1;
    this.totalpages = 1;

    this.filterSelect = 0;

    this.dates1 = '';
    this.datee1 = '';

    this.dates2 = '';
    this.datee2 = '';

    this.list_types = JSON.parse(localStorage.getItem('trip_types'));
    this.tripType = '0';

    this.route.params.subscribe(params => {
      this.filterSelect = params['t'];
      this.type = params['t'];
      switch (this.type) {
        case '1': {
          this.page = +params['z'];
          this.x = params['x'];
          this.y = params['y'];
          this.date1 = {date: {
            year: Number(params['x'].split('-')[2]),
            month: Number(params['x'].split('-')[1]),
            day: Number(params['x'].split('-')[0])}};

          this.date2 = {date: {
            year: Number(params['y'].split('-')[2]),
            month: Number(params['y'].split('-')[1]),
            day: Number(params['y'].split('-')[0])}};
          break;
        }
        case '2': {
          this.page = +params['z'];
          this.x = params['x'];
          this.y = params['y'];

          this.date3 = {date: {
            year: Number(params['x'].split('-')[2]),
            month: Number(params['x'].split('-')[1]),
            day: Number(params['x'].split('-')[0])}};

          this.date4 = {date: {
            year: Number(params['y'].split('-')[2]),
            month: Number(params['y'].split('-')[1]),
            day: Number(params['y'].split('-')[0])}};
          break;
        }
        case '3': {
          this.page = +params['y'];
          this.x = params['x'];
          break;
        }
        case '4': {
          this.page = +params['y'];
          this.x = params['x'];
          this.operacionesv = params['x'];
          break;
        }
        case '5': {
          this.page = +params['y'];
          this.x = params['x'];
          this.gerenciav = params['x'];
          break;
        }
        case '6': {
          this.page = +params['y'];
          this.x = params['x'];
          this.tripType = params['x'];
          break;
        }
        case '7': {
          this.page = +params['y'];
          this.x = params['x'];
          this.code2search = this.x;
          break;
        }
        default: {
          break;
        }
      }


    });

    let params = new FormData();
    params.append('type', this.type);

    if (+this.type > 2) {
      let aux = true;
      if (this.x === '1') {
        aux = false;
      } else {
        aux = true;
      }
      params.append('status', aux ? '1' : '0');
      if (+this.type === 6) {
        params.append('trip_type', this.x);
      }
      if (+this.type === 7) {
        params.append('trip_code', this.x);
      }
    } else {
      const dd = new Date();
      dd.setDate(Number( this.x.split('-')[0]));
      dd.setMonth(Number( this.x.split('-')[1]) - 1);
      dd.setFullYear(Number( this.x.split('-')[2]));

      const dds = new Date();
      dds.setDate(Number( this.y.split('-')[0]));
      dds.setMonth(Number( this.y.split('-')[1]) - 1);
      dds.setFullYear(Number( this.y.split('-')[2]));
      params.append('date_start', dd.getFullYear() + '-' + ( dd.getMonth() + 1 ) + '-' + dd.getDate());
      params.append('date_end', dds.getFullYear() + '-' + ( dds.getMonth() + 1 ) + '-' + dds.getDate());
    }

    this.http.post(localStorage.getItem('baseurl') + 'list_trips/filter/?page=1', params)
      .subscribe(
        response => {
          console.log(response);
          const resaux = JSON.stringify(response);
          this.Viajes = JSON.parse(resaux).objects;
          this.totalpages = JSON.parse(resaux).num_pages;
          this.paginator = [];
          for (let i = 0; i < this.totalpages; i++) {
            if(i + 1 === this.page) {
              this.paginator.push({page:i+1, state: 'active'});
            } else {
              this.paginator.push({page:i+1, state: ''});
            }
          }

          this.createTableRegisters();
          this.isLoading = false;
          this.showData = true;

          if(this.Viajes.length < 1){
            this.showNotFound = 'show-loading';
            this.isLoading = false;
            this.showData = false;
          }
          this.showLoading = 'hide-loading';
          this.showTableData = 'show-data';
        },
        error => {
          console.log(error);
          this.isLoading = false;
          this.showData = false;
        });


  }

  resumeTrip(e) {
    localStorage.setItem('operations', e.operaciones ? '1' : '0');
    localStorage.setItem('management', e.gerencia ? '1' : '0');
    this.router.navigate(['/viajes/resumen/' + e.resumen]);
  }

  export() {
    if (this.selection.hasValue()) {
      const data = [];
      for (let xx of this.selection.selected) {
        data.push(+xx.aux);
      }

      const form = new FormData();
      form.append('type', '9');
      form.append('trips', data.join(','));

      this.http.post(localStorage.getItem('baseurl') + 'export_trips/', form)
        .subscribe(
          response => {
            const resaux = JSON.stringify(response);
            console.log(JSON.parse(resaux));
          },
          error => {
            console.log(error);
            if (error.error.text.indexOf('Desde') !== -1) {
              console.log(error.error.text);
              const blob = new Blob(['\ufeff' + error.error.text], { type: 'text/csv;charset=utf-8;' });
              const dwldLink = document.createElement('a');
              const url = URL.createObjectURL(blob);
              const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
              if (isSafariBrowser) {
                dwldLink.setAttribute('target', '_blank');
              }
              dwldLink.setAttribute('href', url);
              dwldLink.setAttribute('download', 'viajes.csv');
              dwldLink.style.visibility = 'hidden';
              document.body.appendChild(dwldLink);
              dwldLink.click();
              document.body.removeChild(dwldLink);
            }
          });


    } else {
      const form = new FormData();
      form.append('type', this.filterSelect.toString());
      switch (+this.filterSelect) {
        case 1: {
          form.append('date_start', this.date1.date.day + '-' + this.date1.date.month + '-' + this.date1.date.year);
          form.append('date_end', this.date2.date.day + '-' + this.date2.date.month + '-' + this.date2.date.year);
          break;
        }
        case 2: {
          form.append('date_start', this.date3.date.day + '-' + this.date3.date.month + '-' + this.date3.date.year);
          form.append('date_end', this.date4.date.day + '-' + this.date4.date.month + '-' + this.date4.date.year);
          break;
        }
        case 3: {
          form.append('status', this.estadov === '1' ? '0' : '1');
          break;
        }
        case 4: {
          form.append('status', this.operacionesv === '1' ? '0' : '1');
          break;
        }
        case 5: {
          form.append('status', this.gerenciav === '1' ? '0' : '1');
          break;
        }
        case 6: {
          form.append('trip_type', this.tripType);
          break;
        }
        case 7: {
          form.append('trip_code', this.code2search);
          break;
        }
        default: {
          break;
        }
      }

      this.http.post(localStorage.getItem('baseurl') + 'export_trips/', form)
        .subscribe(
          response => {
            const resaux = JSON.stringify(response);
            console.log(JSON.parse(resaux));
          },
          error => {
            console.log(error);
            if (error.error.text.indexOf('Desde') !== -1) {
              console.log(error.error.text);
              const blob = new Blob(['\ufeff' + error.error.text], { type: 'text/csv;charset=utf-8;' });
              const dwldLink = document.createElement('a');
              const url = URL.createObjectURL(blob);
              const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
              if (isSafariBrowser) {
                dwldLink.setAttribute('target', '_blank');
              }
              dwldLink.setAttribute('href', url);
              dwldLink.setAttribute('download', 'viajes.csv');
              dwldLink.style.visibility = 'hidden';
              document.body.appendChild(dwldLink);
              dwldLink.click();
              document.body.removeChild(dwldLink);
            }
          });
    }
  }

  fechas() {
    if (this.fechasFilter !== 0) {
      switch (+this.fechasFilter) {
        case 1: {
          const today = new Date();
          this.date1 = { date: { year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate() } };
          this.date2 = { date: { year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate() } };
          this.date1.formatted = this.date1.date.day + '/' + this.date1.date.month + '/' + this.date1.date.year;
          this.date2.formatted = this.date2.date.day + '/' + this.date2.date.month + '/' + this.date2.date.year;
          this.filtrar();
          break;
        }
        case 2: {
          const yesterday = new Date(new Date().getTime() - 24 * 60 * 60 * 1000);
          this.date2 = { date: { year: yesterday.getFullYear(), month: yesterday.getMonth() + 1, day: yesterday.getDate() } };
          this.date1 = { date: { year: yesterday.getFullYear(), month: yesterday.getMonth() + 1, day: yesterday.getDate() } };
          this.date1.formatted = this.date1.date.day + '/' + this.date1.date.month + '/' + this.date1.date.year;
          this.date2.formatted = this.date2.date.day + '/' + this.date2.date.month + '/' + this.date2.date.year;
          this.filtrar();
          break;
        }
        case 3: {
          const today = new Date();
          const week = today.getDay();
          const monday = week * 24 * 60 * 60 * 1000;
          const saturday = (6 - week) * 24 * 60 * 60 * 1000;
          const mon_date = new Date(today.getTime() - monday);
          const sat_date = new Date(today.getTime() + saturday);
          this.date1 = { date: { year: mon_date.getFullYear(), month: mon_date.getMonth() + 1, day: mon_date.getDate() } };
          this.date2 = { date: { year: sat_date.getFullYear(), month: sat_date.getMonth() + 1, day: sat_date.getDate() } };
          this.date1.formatted = this.date1.date.day + '/' + this.date1.date.month + '/' + this.date1.date.year;
          this.date2.formatted = this.date2.date.day + '/' + this.date2.date.month + '/' + this.date2.date.year;
          this.filtrar();
          break;
        }
        case 4: {
          const today = new Date();
          const first_day = new Date(today.getFullYear(), today.getMonth(), 1);
          const last_day = new Date(today.getFullYear(), today.getMonth() + 1, 0);
          this.date1 = { date: { year: first_day.getFullYear(), month: first_day.getMonth() + 1, day: first_day.getDate() } };
          this.date2 = { date: { year: last_day.getFullYear(), month: last_day.getMonth() + 1, day: last_day.getDate() } };
          this.date1.formatted = this.date1.date.day + '/' + this.date1.date.month + '/' + this.date1.date.year;
          this.date2.formatted = this.date2.date.day + '/' + this.date2.date.month + '/' + this.date2.date.year;
          this.filtrar();
          break;
        }
        case 5: {
          const today = new Date();
          const first_day = new Date(today.getFullYear(), 0, 1);
          const last_day = new Date(today.getFullYear(), 11, 31);
          this.date1 = { date: { year: first_day.getFullYear(), month: first_day.getMonth() + 1, day: first_day.getDate() } };
          this.date2 = { date: { year: last_day.getFullYear(), month: last_day.getMonth() + 1, day: last_day.getDate() } };
          this.date1.formatted = this.date1.date.day + '/' + this.date1.date.month + '/' + this.date1.date.year;
          this.date2.formatted = this.date2.date.day + '/' + this.date2.date.month + '/' + this.date2.date.year;
          this.filtrar();
          break;
        }
        default: {
          break;
        }
      }
    }
  }

  setTimeDisable(event) {
    if (event.formatted === '') {
      this.myDatePickerOptions2 = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
        dayLabels: {
          su: 'Dom',
          mo: 'Lun',
          tu: 'Mar',
          we: 'Mier',
          th: 'Jue',
          fr: 'Vie',
          sa: 'Sab'},
        monthLabels: {
          1: 'Enero',
          2: 'Febrero',
          3: 'Marzo',
          4: 'Abril',
          5: 'Mayo',
          6: 'Junio',
          7: 'Julio',
          8: 'Agosto',
          9: 'Setiembre',
          10: 'Octubre',
          11: 'Noviembre',
          12: 'Diciembre'
        },
        allowDeselectDate: true,
        todayBtnTxt: 'Hoy',
        sunHighlight: true,
        markCurrentDay: true,
        openSelectorOnInputClick: true,
        inline: false,
        editableDateField: true,
        selectorWidth: '400px',
        disableUntil: {year: 0, month: 0, day: 0}
      };
    } else {
      const aa = event.formatted.split('-');
      this.myDatePickerOptions2 = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
        dayLabels: {
          su: 'Dom',
          mo: 'Lun',
          tu: 'Mar',
          we: 'Mier',
          th: 'Jue',
          fr: 'Vie',
          sa: 'Sab'},
        monthLabels: {
          1: 'Enero',
          2: 'Febrero',
          3: 'Marzo',
          4: 'Abril',
          5: 'Mayo',
          6: 'Junio',
          7: 'Julio',
          8: 'Agosto',
          9: 'Setiembre',
          10: 'Octubre',
          11: 'Noviembre',
          12: 'Diciembre'
        },
        allowDeselectDate: true,
        todayBtnTxt: 'Hoy',
        sunHighlight: true,
        markCurrentDay: true,
        openSelectorOnInputClick: true,
        inline: false,
        editableDateField: true,
        selectorWidth: '400px',
        disableUntil: {year: Number(aa[0]), month: Number(aa[1]), day: Number(aa[2])}
      };
    }
  }
  
  selectChange(e: any, row: any) {
    e ? this.selection.toggle(row) : null;


    if (this.selection.hasValue()) {
      this.exportMessage = 'Exportar';
    } else {
      this.exportMessage = 'Exportar Todo';
    }
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));

    if (this.selection.hasValue()) {
      this.exportMessage = 'Exportar';
    } else {
      this.exportMessage = 'Exportar Todo';
    }
  }

  createTableRegisters() {
    this.ELEMENT_DATA = [];

    for ( let xx of this.Viajes) {
      this.ELEMENT_DATA.push(
        {
          planilla: xx.id.toString(),
          resumen: xx.id.toString(),
          codigo: xx.code,
          fecha: xx.date,
          fechafin: xx.date_end,
          operaciones: xx.operations,
          gerencia: xx.management,
          tipo: xx.type,
          calificacion: xx.rate,
          aux: xx.id.toString()
        }
      );
    }

    this.isLoadingResults = false;
    //this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
    this.dataSource = new MatTableDataSource<ViajesAux>(this.ELEMENT_DATA);
    this.selection = new SelectionModel<ViajesAux>(true, []);
    this.dataSource.sort = this.sort;
  }

  getData(x, y, z) {

    this.isLoadingResults = true;

    this.showLoading = 'show-loading';
    this.showTableData = 'hide-data';
    this.showNotFound = 'hide-loading';

    this.isLoading = true;
    this.showData = false;

    this.page = 1;
    this.totalpages = 1;


      this.type = x.toString();
      switch (this.type) {
        case '1': {
          this.page = 1;
          this.x = y.toString();
          this.y = z;
          break;
        }
        case '2': {
          this.page = 1;
          this.x = y.toString();
          this.y = z;
          break;
        }
        case '3': {
          this.page = 1;
          this.x = y.toString();
          break;
        }
        case '4': {
          this.page = 1;
          this.x = y.toString();
          break;
        }
        case '5': {
          this.page = 1;
          this.x = y.toString();
          break;
        }
        case '6': {
          this.page = 1;
          this.x = y.toString();
          break;
        }
        case '7': {
          this.page = 1;
          this.x = y.toString();
          break;
        }
        default: {
          break;
        }
      }


    let params = new FormData();
    params.append('type', this.type);

    if (+this.type > 2) {
      let aux = true;
      if (this.x === '1') {
        aux = false;
      } else {
        aux = true;
      }

      params.append('status', aux ? '1' : '0');
      if (+this.type === 6) {
        params.append('trip_type', this.x);
      }
      if (+this.type === 7) {
        params.append('trip_code', this.x);
      }
    } else {
      const dd = new Date();
      dd.setDate(Number( this.x.split('-')[0]));
      dd.setMonth(Number( this.x.split('-')[1]) - 1);
      dd.setFullYear(Number( this.x.split('-')[2]));

      const dds = new Date();
      dds.setDate(Number( this.y.split('-')[0]));
      dds.setMonth(Number( this.y.split('-')[1]) - 1);
      dds.setFullYear(Number( this.y.split('-')[2]));
      params.append('date_start', dd.getFullYear() + '-' + (dd.getMonth() + 1) + '-' + dd.getDate());
      params.append('date_end', dds.getFullYear() + '-' + (dds.getMonth() + 1) + '-' + dds.getDate());
    }

    this.http.post('http://amex.infoloop.net/web/list_trips/filter/?page=1', params)
      .subscribe(
        response => {
          console.log(response);
          const resaux = JSON.stringify(response);
          this.Viajes = JSON.parse(resaux).objects;
          this.totalpages = JSON.parse(resaux).num_pages;
          this.paginator = [];
          for (let i = 0; i < this.totalpages; i++) {
            if(i + 1 === this.page) {
              this.paginator.push({page:i+1, state: 'active'});
            } else {
              this.paginator.push({page:i+1, state: ''});
            }
          }

          this.createTableRegisters();
          this.isLoading = false;
          this.showData = true;
          if(this.Viajes.length < 1){
            this.showNotFound = 'show-loading';
            this.isLoading = false;
            this.showData = false;
          }
          this.showLoading = 'hide-loading';
          this.showTableData = 'show-data';
        },
        error => {
          console.log(error);
          this.isLoading = false;
          this.showData = false;
        });

  }

  searchCode() {
    this.filterSelect = 7;
    this.filtrar();
  }

  filtrar() {
    if (this.date1 !== undefined) {
      if (this.date1.formatted !== undefined) {
        this.dates1 = this.date1.formatted.split('/').join('-');
      }
    }
    if(this.date2 !== undefined) {
      if(this.date2.formatted !== undefined) {
        this.datee1 = this.date2.formatted.split('/').join('-');
      }
    }

    if(this.date3 !== undefined) {
      if(this.date3.formatted !== undefined) {
        this.dates2 = this.date3.formatted.split('/').join('-');
      }
    }
    if(this.date4 !== undefined) {
      if(this.date4.formatted !== undefined) {
        this.datee2 = this.date4.formatted.split('/').join('-');
      }
    }


    switch (+this.filterSelect) {
      case 1: {
        if(this.datee1 !== '' || this.dates1 !== '') {
          this.location.replaceState("/viajes/filter/1/" + this.dates1 + '/' + this.datee1 + '/' + 1);
          this.getData(1, this.dates1, this.datee1);
        }
        break;
      }
      case 2:{
        if(this.datee2 !== '' || this.dates2 !== '') {
          this.location.replaceState("/viajes/filter/2/" + this.dates2 + '/' + this.datee2 + '/' + 1);
          this.getData(2,this.dates2,this.datee2);
        }
        break;
      }
      case 3:{
        this.location.replaceState("/viajes/filter/3/"+this.estadov+'/1');
        this.getData(3,this.estadov,0);
        break;
      }
      case 4:{
        this.location.replaceState("/viajes/filter/4/"+this.operacionesv+'/1');
        this.getData(4,this.operacionesv,0);
        break;
      }
      case 5:{
        this.location.replaceState("/viajes/filter/5/"+this.gerenciav+'/1');
        this.getData(5,this.gerenciav,0);
        break;
      }
      case 6:{
        if( this.tripType !== '0'){
          this.location.replaceState("/viajes/filter/6/"+this.tripType+'/1');
          this.getData(6,this.tripType,0);
        }
        break;
      }
      case 7:{
        if( this.code2search !== ''){
          this.location.replaceState("/viajes/filter/7/"+this.code2search+'/1');
          this.getData(7,this.code2search,0);
        } else {
          this.location.back();
        }
        break;
      }
      default:
        // code...
        break;
    }
  }

  changePage(p) {
    this.isLoadingResults = true;
    this.showLoading = 'show-loading';
    this.showTableData = 'hide-data';
    this.isLoading = true;
    this.showData = false;
    for(let i of this.paginator) {
      i.state = '';
      if (i.page == p) {
        i.state = 'active';
      }
    }
    this.page = p;
    if(this.page == 1) {
      this.disablePrev = 'disabled';
    }
    if(this.page == this.totalpages) {
      this.disableNext = 'disabled';
    }
    if(this.page > 1) {
      this.disablePrev = '';
    }
    if(this.page < this.totalpages) {
      this.disableNext = '';
    }

    let params = {};

    if (+this.type > 2) {
      let aux = true;
      if (this.x === '1') {
        aux = false;
      } else {
        aux = true;
      }
      params = {
        'type': this.type,
        'status': aux,
      };
      if (+this.type === 6) {
        params = {
          'type': this.type,
          'trip_type': this.x,
        };
      }
      if (+this.type === 7) {
        params = {
          'type': this.type,
          'trip_code': this.x,
        };
      }
    } else {
      const dd = new Date();
      dd.setDate(Number( this.x.split('-')[0]));
      dd.setMonth(Number( this.x.split('-')[1]) - 1);
      dd.setFullYear(Number( this.x.split('-')[2]));

      const dds = new Date();
      dds.setDate(Number( this.y.split('-')[0]));
      dds.setMonth(Number( this.y.split('-')[1]) - 1);
      dds.setFullYear(Number( this.y.split('-')[2]));

      params = {
        'type': this.type,
        'date_start': dd.getFullYear() + '-' + dd.getMonth() + '-' + dd.getDate(),
        'date_end': dds.getFullYear() + '-' + dds.getMonth() + '-' + dds.getDate()
      };
    }

    if (+this.type > 2) {
      this.location.replaceState('/viajes/filter/' + this.type + '/' + this.x + '/' + this.page);
    } else {
      this.location.replaceState('/viajes/filter/' + this.type + '/' + this.x.split('/').join('-') + '/' + this.y.split('/').join('-') + '/' + this.page);
    }



    this.http.post(localStorage.getItem('baseurl') + 'list_trips/filter/?page=' + this.page, params)
      .subscribe(
        response => {
          const resaux = JSON.stringify(response);
          this.Viajes = JSON.parse(resaux).objects;
          this.showLoading = 'hide-loading';
          this.showTableData = 'show-data';
          this.isLoading = false;
          this.showData = true;
          this.createTableRegisters();
        },
        error => {
          console.log(error);
        });

  }

  editTrip(v) {
    if (this.showedit) {
      let vv;
      for (let xx of this.Viajes) {
        if (xx.id.toString() === v) {
          vv = xx;
          localStorage.setItem("edittrip", JSON.stringify(vv));
          this.router.navigate(['/viajes/edit/'+v]);
          return;
        }
      }
    }
  }
  viewTrip(v) {
    let vv;
    for (let xx of this.Viajes) {
      if (xx.id.toString() === v) {
        vv = xx;
        localStorage.setItem("edittrip", JSON.stringify(vv));
        this.router.navigate(['/viajes/view/'+v]);
        return;
      }
    }
  }
  deleteTrip(v) {
    let vv;
    for (let xx of this.Viajes) {
      if (xx.id.toString() === v) {
        vv = xx;
        break;
      }
    }
    this.modalService.openDialog(this.viewRef, {
      title: '¿Seguro que quieres eliminar el Viaje?',
      childComponent: SimpleModalComponent,
      data: {
        text: ''
      },
      settings: {
        closeButtonClass: 'close theme-icon-close'
      },
      actionButtons: [
        {
          text: 'Si',
          buttonClass: 'btn btn-danger',
          onAction: () => new Promise((resolve: any, reject: any) => {
            setTimeout(() => {
              const formData = new FormData();
              formData.append('id', v);
              this.http.post(localStorage.getItem('baseurl') + 'list_trips/delete/', formData)
                .subscribe(
                  res => {
                    console.log(res);
                    //this.location.back();
                  },
                  error => {
                    console.log(error.error.text);
                    if(error.error.text === 'Trip deleted.') {
                      resolve();
                      this.Viajes = this.Viajes.filter(obj => obj !== vv);
                      this.isLoadingResults = true;
                      this.createTableRegisters();
                    }
                  }
                );
            }, 20);
          })
        },
        {
          text: 'No',
          buttonClass: 'btn btn-success',
          onAction: () => new Promise((resolve: any) => {
            setTimeout(() => {
              resolve();
            }, 20);
          })
        },
      ]
    });

  }
  activeTrip(v) {
    for(let x of this.Viajes) {
      x.active = '';
    }
    v.active = 'active';
  }

  previusPage() {
    this.changePage(this.page - 1);
  }

  nextPage() {
    this.changePage(this.page + 1);
  }

  filtrarForm(e, val) {
  	switch (val) {
  		case "0":{
  			this.fecha = 'no-display';
			this.fechaFin = 'no-display';
			this.operaciones = 'no-display';
			this.gerencia = 'no-display';
			this.tipoViaje = 'no-display';
        this.estado = 'no-display';
  			break;
  		}
  		case "1":{
  			this.fecha = '';
			this.fechaFin = 'no-display';
			this.operaciones = 'no-display';
			this.gerencia = 'no-display';
			this.tipoViaje = 'no-display';
        this.estado = 'no-display';
  			break;
  		}
  		case "2":{
  			this.fecha = 'no-display';
			this.fechaFin = '';
			this.operaciones = 'no-display';
			this.gerencia = 'no-display';
			this.tipoViaje = 'no-display';
        this.estado = 'no-display';
  			break;
  		}
      case "3":{
        this.fecha = 'no-display';
        this.fechaFin = 'no-display';
        this.operaciones = 'no-display';
        this.gerencia = 'no-display';
        this.tipoViaje = 'no-display';
        this.estado = '';
        break;
      }
  		case "4":{
  			this.fecha = 'no-display';
			this.fechaFin = 'no-display';
			this.operaciones = '';
			this.gerencia = 'no-display';
			this.tipoViaje = 'no-display';
        this.estado = 'no-display';
  			break;
  		}
  		case "5":{
  			this.fecha = 'no-display';
			this.fechaFin = 'no-display';
			this.operaciones = 'no-display';
			this.gerencia = '';
			this.tipoViaje = 'no-display';
        this.estado = 'no-display';
  			break;
  		}
  		case "6":{
  			this.fecha = 'no-display';
			this.fechaFin = 'no-display';
			this.operaciones = 'no-display';
			this.gerencia = 'no-display';
        this.estado = 'no-display';
			this.tipoViaje = '';
  			break;
  		}

  		default:
  			// code...
  			break;
  	}
  }

}
