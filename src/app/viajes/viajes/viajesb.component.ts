import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';
import { MatSort, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

import { Page } from '../../common/page';
import { ViajesAux } from '../../common/viajeaux';
import { Trip } from '../../common/trip';
import {IMyDpOptions} from 'mydatepicker';


@Component({
  selector: 'app-viajes-viajes',
  templateUrl: './viajesb.component.html',
  styleUrls: ['./viajesb.component.css']
})

export class ViajesBComponent implements OnInit {

  displayedColumns = [
    'select',
    'resumen',
    'codigo',
    'fecha',
    'fechafin',
    'operaciones',
    'gerencia',
    'tipo',
    'calificacion',
    'aux'
  ];
  dataSource;

  baseimg2: string = localStorage.getItem('baseassets');

  ELEMENT_DATA: ViajesAux[];

  @ViewChild(MatSort) sort: MatSort;

  selection;

  fecha: string;
	fechaFin: string;
	operaciones: string;
	gerencia: string;
	tipoViaje: string;
  estado: string;

  Viajes: Trip[];
  page: number;
  totalpages: number;
  paginator: Page[];

  disablePrev: string;
  disableNext: string;

  showLoading: string;
  showTableData: string;

  filterSelect: number;

  isLoadingResults: boolean;

  activeExport: string;

  dates1: string;
  datee1: string;

  dates2: string;
  datee2: string;

  estadov: string;
  operacionesv: string;
  gerenciav: string;

  list_types: any[];
  tripType: string;

  code2search: string;

  showNotFound: string;

  isLoading = true;
  showData = false;

  isExporting = false;

  fechasFilter = 0;

  export_message = 'Exportar Todo';

  rol = JSON.parse(localStorage.getItem('rol'));
  showadd;
  showview;
  showedit;
  showdelete;
  showexport;
  showabstract;

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'dd/mm/yyyy',
    dayLabels: {
      su: 'Dom',
      mo: 'Lun',
      tu: 'Mar',
      we: 'Mier',
      th: 'Jue',
      fr: 'Vie',
      sa: 'Sab'},
    monthLabels: {
      1: 'Enero',
      2: 'Febrero',
      3: 'Marzo',
      4: 'Abril',
      5: 'Mayo',
      6: 'Junio',
      7: 'Julio',
      8: 'Agosto',
      9: 'Setiembre',
      10: 'Octubre',
      11: 'Noviembre',
      12: 'Diciembre'
    },
    allowDeselectDate: true,
    todayBtnTxt: 'Hoy',
    sunHighlight: true,
    markCurrentDay: true,
    openSelectorOnInputClick: true,
    inline: false,
    editableDateField: true,
    selectorWidth: '400px'
  };

  public myDatePickerOptions2: IMyDpOptions = {
    // other options...
    dateFormat: 'dd/mm/yyyy',
    dayLabels: {
      su: 'Dom',
      mo: 'Lun',
      tu: 'Mar',
      we: 'Mier',
      th: 'Jue',
      fr: 'Vie',
      sa: 'Sab'},
    monthLabels: {
      1: 'Enero',
      2: 'Febrero',
      3: 'Marzo',
      4: 'Abril',
      5: 'Mayo',
      6: 'Junio',
      7: 'Julio',
      8: 'Agosto',
      9: 'Setiembre',
      10: 'Octubre',
      11: 'Noviembre',
      12: 'Diciembre'
    },
    allowDeselectDate: true,
    todayBtnTxt: 'Hoy',
    sunHighlight: true,
    markCurrentDay: true,
    openSelectorOnInputClick: true,
    inline: false,
    editableDateField: true,
    selectorWidth: '400px'
  };

  public date1: any;
  public date2: any;
  public date3: any;
  public date4: any;

  items: Number;
  itemspertable = 20;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private http: HttpClient,
    private modalService: ModalDialogService,
    private viewRef: ViewContainerRef
    ) { }

  ngOnInit() {

    const formData = new FormData();
    formData.append('username', localStorage.getItem('username'));

    this.http.post(localStorage.getItem('baseurl') + 'getuser/', formData)
      .subscribe(
        res => {
          this.rol = JSON.parse(JSON.parse(JSON.stringify(res)).role)[0].fields;
          localStorage.setItem('rol', JSON.stringify(this.rol));
          this.showadd = this.rol.trip_add;
          this.showview = this.rol.trip_view;
          this.showedit = this.rol.trip_edit;
          this.showdelete = this.rol.trip_delete;
          this.showexport = this.rol.trip_export;
          this.showabstract = this.rol.trip_abstract;

          if (!this.showabstract) {
            this.displayedColumns = this.displayedColumns.filter(nam => nam !== 'resumen');
          }
        },
        error => {
          console.log(error);
        }
      );

    this.activeExport = 'disabled';
    this.isLoadingResults = true;
    this.list_types = JSON.parse(localStorage.getItem('trip_types'));
    this.tripType = '0';

    this.fecha = 'no-display';
    this.fechaFin = 'no-display';
    this.operaciones = 'no-display';
    this.gerencia = 'no-display';
    this.tipoViaje = 'no-display';
    this.estado = 'no-display';

    this.showLoading = 'show-loading';
    this.showTableData = 'hide-data';
    this.showNotFound = 'hide-loading';

    this.disablePrev = 'disabled';
    this.disableNext = '';

    this.page = 1;
    this.totalpages = 1;
    this.filterSelect = 0;

    this.dates1 = '';
    this.datee1 = '';

    this.dates2 = '';
    this.datee2 = '';


    this.route.params.subscribe(params => {
      this.page = +params['p'];
    });


    this.http.get(localStorage.getItem('baseurl') + 'list_trips/?page=' + this.page)
      .subscribe(
        response => {
          const resaux = JSON.stringify(response);
          this.totalpages = JSON.parse(resaux).num_pages;
          this.items = JSON.parse(resaux).items;
          this.Viajes = JSON.parse(resaux).objects;
          this.Viajes = this.Viajes.filter( obj => obj.operations === false && obj.management === false );
          this.paginator = [];

          for (let i = 0; i < this.totalpages; i++) {
            if(i+1 == this.page) {
              this.paginator.push({page:i+1, state: 'active'});
            } else {
              this.paginator.push({page:i+1, state: ''});
            }
          }

          this.createTableRegisters();

          this.showLoading = 'hide-loading';
          this.showTableData = 'show-data';
          this.isLoading = false;
          this.showData = true;

          if(this.Viajes.length < 1) {
            this.showNotFound = 'show-loading';
            this.isLoading = false;
            this.showData = false;
          }
        },
        error => {
          console.log(error);
          this.isLoading = false;
          this.showData = false;
        });
  }

  fechas() {
    if (this.fechasFilter !== 0) {
      switch (+this.fechasFilter) {
        case 1: {
          const today = new Date();
          this.date1 = { date: { year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate() } };
          this.date2 = { date: { year: today.getFullYear(), month: today.getMonth() + 1, day: today.getDate() } };
          this.date1.formatted = this.date1.date.day + '/' + this.date1.date.month + '/' + this.date1.date.year;
          this.date2.formatted = this.date2.date.day + '/' + this.date2.date.month + '/' + this.date2.date.year;
          this.filtrar();
          break;
        }
        case 2: {
          const yesterday = new Date(new Date().getTime() - 24 * 60 * 60 * 1000);
          this.date2 = { date: { year: yesterday.getFullYear(), month: yesterday.getMonth() + 1, day: yesterday.getDate() } };
          this.date1 = { date: { year: yesterday.getFullYear(), month: yesterday.getMonth() + 1, day: yesterday.getDate() } };
          this.date1.formatted = this.date1.date.day + '/' + this.date1.date.month + '/' + this.date1.date.year;
          this.date2.formatted = this.date2.date.day + '/' + this.date2.date.month + '/' + this.date2.date.year;
          this.filtrar();
          break;
        }
        case 3: {
          const today = new Date();
          const week = today.getDay();
          const monday = week * 24 * 60 * 60 * 1000;
          const saturday = (6 - week) * 24 * 60 * 60 * 1000;
          const mon_date = new Date(today.getTime() - monday);
          const sat_date = new Date(today.getTime() + saturday);
          this.date1 = { date: { year: mon_date.getFullYear(), month: mon_date.getMonth() + 1, day: mon_date.getDate() } };
          this.date2 = { date: { year: sat_date.getFullYear(), month: sat_date.getMonth() + 1, day: sat_date.getDate() } };
          this.date1.formatted = this.date1.date.day + '/' + this.date1.date.month + '/' + this.date1.date.year;
          this.date2.formatted = this.date2.date.day + '/' + this.date2.date.month + '/' + this.date2.date.year;
          this.filtrar();
          break;
        }
        case 4: {
          const today = new Date();
          const first_day = new Date(today.getFullYear(), today.getMonth(), 1);
          const last_day = new Date(today.getFullYear(), today.getMonth() + 1, 0);
          this.date1 = { date: { year: first_day.getFullYear(), month: first_day.getMonth() + 1, day: first_day.getDate() } };
          this.date2 = { date: { year: last_day.getFullYear(), month: last_day.getMonth() + 1, day: last_day.getDate() } };
          this.date1.formatted = this.date1.date.day + '/' + this.date1.date.month + '/' + this.date1.date.year;
          this.date2.formatted = this.date2.date.day + '/' + this.date2.date.month + '/' + this.date2.date.year;
          this.filtrar();
          break;
        }
        case 5: {
          const today = new Date();
          const first_day = new Date(today.getFullYear(), 0, 1);
          const last_day = new Date(today.getFullYear(), 11, 31);
          this.date1 = { date: { year: first_day.getFullYear(), month: first_day.getMonth() + 1, day: first_day.getDate() } };
          this.date2 = { date: { year: last_day.getFullYear(), month: last_day.getMonth() + 1, day: last_day.getDate() } };
          this.date1.formatted = this.date1.date.day + '/' + this.date1.date.month + '/' + this.date1.date.year;
          this.date2.formatted = this.date2.date.day + '/' + this.date2.date.month + '/' + this.date2.date.year;
          this.filtrar();
          break;
        }
        default: {
          break;
        }
      }
    }
  }

  setRows() {
    this.showLoading = 'show-loading';
    this.showTableData = 'hide-data';
    this.showNotFound = 'hide-loading';

    this.disablePrev = 'disabled';
    this.disableNext = '';

    this.isLoading = true;
    this.showData = false;


    this.page = 1;
    this.totalpages = 1;
    this.filterSelect = 0;
    this.location.replaceState('/viajes/' + this.page);
    this.http.get(localStorage.getItem('baseurl') + 'list_trips/?page=' + this.page + '&items=' + this.itemspertable)
      .subscribe(
        response => {
          const resaux = JSON.stringify(response);
          this.totalpages = JSON.parse(resaux).num_pages;
          this.Viajes = JSON.parse(resaux).objects;
          this.Viajes = this.Viajes.filter( obj => obj.operations === false && obj.management === false );
          this.paginator = [];

          for (let i = 0; i < this.totalpages; i++) {
            if(i+1 == this.page) {
              this.paginator.push({page:i+1, state: 'active'});
            } else {
              this.paginator.push({page:i+1, state: ''});
            }
          }

          this.createTableRegisters();

          this.showLoading = 'hide-loading';
          this.showTableData = 'show-data';
          this.isLoading = false;
          this.showData = true;

          if (this.Viajes.length < 1) {
            this.showNotFound = 'show-loading';
            this.isLoading = false;
            this.showData = false;
          }
        },
        error => {
          console.log(error);
          this.isLoading = false;
          this.showData = false;
        });
  }

  export() {
    this.isExporting = true;
    if(this.selection.hasValue()) {
      const data = [];
      for(let xx of this.selection.selected) {
        data.push(+xx.aux);
      }

      const form = new FormData();
      form.append('type', '9');
      form.append('trips', data.join(','));

      this.http.post(localStorage.getItem('baseurl') + 'export_trips/', form)
        .subscribe(
          response => {
            const resaux = JSON.stringify(response);
            console.log(JSON.parse(resaux));
          },
          error => {
            console.log(error);
            if (error.error.text.indexOf('Desde') !== -1) {
              console.log(error.error.text);
              const blob = new Blob(['\ufeff' + error.error.text], { type: 'text/csv;charset=utf-8;' });
              const dwldLink = document.createElement('a');
              const url = URL.createObjectURL(blob);
              const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
              if (isSafariBrowser) {
                dwldLink.setAttribute('target', '_blank');
              }
              dwldLink.setAttribute('href', url);
              dwldLink.setAttribute('download', 'viajes.csv');
              dwldLink.style.visibility = 'hidden';
              document.body.appendChild(dwldLink);
              dwldLink.click();
              document.body.removeChild(dwldLink);
              this.isExporting = false;
            }
          });
    } else {
      const form = new FormData();
      form.append('type', '0');
      this.http.post(localStorage.getItem('baseurl') + 'export_trips/', form)
        .subscribe(
          response => {
            const resaux = JSON.stringify(response);
            console.log(JSON.parse(resaux));
          },
          error => {
            console.log(error);
            if (error.error.text.indexOf('Desde') !== -1) {
              console.log(error.error.text);
              const blob = new Blob(['\ufeff' + error.error.text], { type: 'text/csv;charset=utf-8;' });
              const dwldLink = document.createElement('a');
              const url = URL.createObjectURL(blob);
              const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
              if (isSafariBrowser) {
                dwldLink.setAttribute('target', '_blank');
              }
              dwldLink.setAttribute('href', url);
              dwldLink.setAttribute('download', 'viajes.csv');
              dwldLink.style.visibility = 'hidden';
              document.body.appendChild(dwldLink);
              dwldLink.click();
              document.body.removeChild(dwldLink);
              this.isExporting = false;
            }
          });
    }
  }

  setTimeDisable(event) {
    if (event.formatted === '') {
      this.myDatePickerOptions2 = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
        dayLabels: {
          su: 'Dom',
          mo: 'Lun',
          tu: 'Mar',
          we: 'Mier',
          th: 'Jue',
          fr: 'Vie',
          sa: 'Sab'},
        monthLabels: {
          1: 'Enero',
          2: 'Febrero',
          3: 'Marzo',
          4: 'Abril',
          5: 'Mayo',
          6: 'Junio',
          7: 'Julio',
          8: 'Agosto',
          9: 'Setiembre',
          10: 'Octubre',
          11: 'Noviembre',
          12: 'Diciembre'
        },
        allowDeselectDate: true,
        todayBtnTxt: 'Hoy',
        sunHighlight: true,
        markCurrentDay: true,
        openSelectorOnInputClick: true,
        inline: false,
        editableDateField: true,
        selectorWidth: '400px',
        disableUntil: {year: 0, month: 0, day: 0}
      };
    } else {
      const aa = event.formatted.split('-');
      this.myDatePickerOptions2 = {
        // other options...
        dateFormat: 'dd/mm/yyyy',
        dayLabels: {
          su: 'Dom',
          mo: 'Lun',
          tu: 'Mar',
          we: 'Mier',
          th: 'Jue',
          fr: 'Vie',
          sa: 'Sab'},
        monthLabels: {
          1: 'Enero',
          2: 'Febrero',
          3: 'Marzo',
          4: 'Abril',
          5: 'Mayo',
          6: 'Junio',
          7: 'Julio',
          8: 'Agosto',
          9: 'Setiembre',
          10: 'Octubre',
          11: 'Noviembre',
          12: 'Diciembre'
        },
        allowDeselectDate: true,
        todayBtnTxt: 'Hoy',
        sunHighlight: true,
        markCurrentDay: true,
        openSelectorOnInputClick: true,
        inline: false,
        editableDateField: true,
        selectorWidth: '400px',
        disableUntil: {year: Number(aa[0]), month: Number(aa[1]), day: Number(aa[2])}
      };
    }
  }

  selectChange(e: any, row: any) {
    e ? this.selection.toggle(row) : null;


    if(this.selection.hasValue()){
      this.export_message = 'Exportar';
    } else {
      this.export_message = 'Exportar Todo';
    }
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));

      if(this.selection.hasValue()){
        this.export_message = 'Exportar';
      } else {
        this.export_message = 'Exportar Todo';
      }
  }

  changePage(p) {
    this.showLoading = 'show-loading';
    this.showTableData = 'hide-data';
    this.showNotFound = 'hide-loading';
    this.isLoadingResults = true;
    this.isLoading = true;
    this.showData = false;

    for(let i of this.paginator) {
      i.state = '';
      if (i.page == p) {
        i.state = 'active';
      }
    }
    this.page = p;
    if(this.page == 1) {
      this.disablePrev = 'disabled';
    }
    if(this.page == this.totalpages) {
      this.disableNext = 'disabled';
    }
    if(this.page > 1) {
      this.disablePrev = '';
    }
    if(this.page < this.totalpages) {
      this.disableNext = '';
    }

    this.location.replaceState("/viajes/"+this.page);

    this.http.get(localStorage.getItem('baseurl') + 'list_trips/?page='+this.page)
      .subscribe(
        response => {
          const resaux = JSON.stringify(response);
          this.totalpages = JSON.parse(resaux).num_pages;
          this.Viajes = JSON.parse(resaux).objects;

          this.createTableRegisters();

          this.showLoading = 'hide-loading';
          this.showTableData = 'show-data';
          this.isLoading = false;
          this.showData = true;

          if (this.Viajes.length < 1) {
            this.showNotFound = 'show-loading';
            this.isLoading = false;
            this.showData = false;
          }
        },
        error => {
          console.log(error);
          this.isLoading = false;
          this.showData = false;
        });
  }

  createTableRegisters() {
    this.ELEMENT_DATA = [];

    for ( let xx of this.Viajes) {
      this.ELEMENT_DATA.push(
        {
          planilla: xx.id.toString(),
          resumen: xx.id.toString(),
          codigo: xx.code,
          fecha: xx.date,
          fechafin: xx.date_end,
          operaciones: xx.operations,
          gerencia: xx.management,
          tipo: xx.type,
          calificacion: xx.rate,
          aux: xx.id.toString()
        }
      );
    }

    this.isLoadingResults = false;
    //this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
    this.dataSource = new MatTableDataSource<ViajesAux>(this.ELEMENT_DATA);
    this.selection = new SelectionModel<ViajesAux>(true, []);
    this.dataSource.sort = this.sort;
  }

  editTrip(v) {
    if (this.showedit) {
      let vv;
      for (let xx of this.Viajes) {
        if (xx.id.toString() === v) {
          vv = xx;
          localStorage.setItem("edittrip", JSON.stringify(vv));
          this.router.navigate(['/viajes/edit/'+v]);
          return;
        }
      }
    }
  }
  viewTrip(v) {
    let vv;
    for (let xx of this.Viajes) {
      if (xx.id.toString() === v) {
        vv = xx;
        localStorage.setItem("edittrip", JSON.stringify(vv));
        this.router.navigate(['/viajes/view/'+v]);
        return;
      }
    }
  }

  deleteTrip(v) {
    let vv;
    for (let xx of this.Viajes) {
      if (xx.id.toString() === v) {
        vv = xx;
        break;
      }
    }
    this.modalService.openDialog(this.viewRef, {
      title: '¿Seguro que quieres eliminar el Viaje?',
      childComponent: SimpleModalComponent,
      data: {
        text: ''
      },
      settings: {
        closeButtonClass: 'close theme-icon-close'
      },
      actionButtons: [
        {
          text: 'Si',
          buttonClass: 'btn btn-danger',
          onAction: () => new Promise((resolve: any, reject: any) => {
            setTimeout(() => {
              const formData = new FormData();
              formData.append('id', v);
              this.http.post(localStorage.getItem('baseurl') + 'list_trips/delete/', formData)
                .subscribe(
                  res => {
                    console.log(res);
                    //this.location.back();
                  },
                  error => {
                    console.log(error.error.text);
                    if(error.error.text === 'Trip deleted.') {
                      resolve();
                      this.Viajes = this.Viajes.filter(obj => obj !== vv);
                      this.isLoadingResults = true;
                      this.createTableRegisters();
                    }
                  }
                );
            }, 20);
          })
        },
        {
          text: 'No',
          buttonClass: 'btn btn-success',
          onAction: () => new Promise((resolve: any) => {
            setTimeout(() => {
              resolve();
            }, 20);
          })
        },
      ]
    });

  }
  activeTrip(v) {
    for(let x of this.Viajes) {
      x.active = '';
    }
    v.active = 'active';
  }

  resumeTrip(e) {
    localStorage.setItem('operations', e.operaciones ? '1' : '0');
    localStorage.setItem('management', e.gerencia ? '1' : '0');
    this.router.navigate(['/viajes/resumen/' + e.resumen]);
  }

  filtrar() {

    if (this.date1 !== undefined) {
      if (this.date1.formatted !== undefined) {
        this.dates1 = this.date1.formatted.split('/').join('-');
      }
    }
    if (this.date2 !== undefined) {
      if (this.date2.formatted !== undefined) {
        this.datee1 = this.date2.formatted.split('/').join('-');
      }
    }

    if (this.date3 !== undefined) {
      if (this.date3.formatted !== undefined) {
        this.dates2 = this.date3.formatted.split('/').join('-');
      }
    }

    if (this.date4 !== undefined) {
      if (this.date4.formatted !== undefined) {
        this.datee2 = this.date4.formatted.split('/').join('-');
      }
    }

    switch (+this.filterSelect) {
      case 1: {
        if (this.dates1 !== '' || this.datee1 !== '') {
          this.router.navigate(['/viajes/filter/1/' + this.dates1 + '/' + this.datee1 + '/1']);
        }
        break;
      }
      case 2: {
        if (this.dates2 !== '' || this.datee2 !== '') {
          this.router.navigate(['/viajes/filter/2/' + this.dates2 + '/' + this.datee2 + '/1']);
        }
        break;
      }
      case 3:{
        this.router.navigate(['/viajes/filter/3/'+this.estadov+'/1']);
        break;
      }
      case 4:{
        this.router.navigate(['/viajes/filter/4/'+this.operacionesv+'/1']);
        break;
      }
      case 5:{
        this.router.navigate(['/viajes/filter/5/'+this.gerenciav+'/1']);
        break;
      }
      case 6:{
        if( this.tripType !== '0'){
          this.router.navigate(['/viajes/filter/6/'+this.tripType+'/1']);
        }
        break;
      }
      default:
        // code...
        break;
    }
  }

  previusPage() {
    this.changePage(this.page - 1);
  }

  nextPage() {
    this.changePage(this.page + 1);
  }

  searchCode() {
    this.router.navigate(['/viajes/filter/7/'+this.code2search+'/1']);
  }

  filtrarForm(e, val) {
  	switch (val) {
  		case "0":{
  			this.fecha = 'no-display';
			this.fechaFin = 'no-display';
			this.operaciones = 'no-display';
			this.gerencia = 'no-display';
			this.tipoViaje = 'no-display';
      this.estado = 'no-display';
  			break;
  		}
  		case "1":{
  			this.fecha = '';
			this.fechaFin = 'no-display';
			this.operaciones = 'no-display';
			this.gerencia = 'no-display';
			this.tipoViaje = 'no-display';
      this.estado = 'no-display';
  			break;
  		}
  		case "2":{
  			this.fecha = 'no-display';
			this.fechaFin = '';
			this.operaciones = 'no-display';
			this.gerencia = 'no-display';
			this.tipoViaje = 'no-display';
      this.estado = 'no-display';
  			break;
  		}
      case "3":{
        this.fecha = 'no-display';
      this.fechaFin = 'no-display';
      this.operaciones = 'no-display';
      this.gerencia = 'no-display';
      this.tipoViaje = 'no-display';
      this.estado = '';
        break;
      }
  		case "4":{
  			this.fecha = 'no-display';
			this.fechaFin = 'no-display';
			this.operaciones = '';
			this.gerencia = 'no-display';
			this.tipoViaje = 'no-display';
      this.estado = 'no-display';
  			break;
  		}
  		case "5":{
  			this.fecha = 'no-display';
			this.fechaFin = 'no-display';
			this.operaciones = 'no-display';
			this.gerencia = '';
			this.tipoViaje = 'no-display';
      this.estado = 'no-display';
  			break;
  		}
  		case "6":{
  			this.fecha = 'no-display';
			this.fechaFin = 'no-display';
			this.operaciones = 'no-display';
			this.gerencia = 'no-display';
			this.tipoViaje = '';
      this.estado = 'no-display';
  			break;
  		}

  		default:
  			// code...
  			break;
  	}
  }

}
