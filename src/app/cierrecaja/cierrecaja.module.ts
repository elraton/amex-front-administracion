import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { CierreCajaComponent } from './cierrecaja.component';
import { CierreCajaRoutes } from './cierrecaja.routing';
import { SharedModule } from '../shared/shared.module';

import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule, MatProgressSpinnerModule, MatCheckboxModule, MatSelectModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatInputModule} from '@angular/material';

import { MyDatePickerModule } from 'mydatepicker';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(CierreCajaRoutes),
      SharedModule,
      MatTabsModule,
      MatTableModule,
      MatSortModule,
      MatProgressSpinnerModule,
      FormsModule,
      ReactiveFormsModule,
      MatCheckboxModule,
      MatSelectModule,
      MyDatePickerModule,
      MatAutocompleteModule,
      MatInputModule
  ],
  declarations: [
  	CierreCajaComponent,
  ]
})

export class CierreCajaModule {}
