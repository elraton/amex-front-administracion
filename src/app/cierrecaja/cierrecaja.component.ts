import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';

import { Page } from '../common/page';
import { IMyDpOptions } from 'mydatepicker';
import { MatSort, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

import { CashClosed } from '../common/cashclosed';
import { CashNotClosed } from '../common/cashnotclosed';

import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';

@Component({
  selector: 'app-viajes-viajes',
  templateUrl: './cierrecaja.component.html',
  styleUrls: ['./cierrecaja.component.css']
})
export class CierreCajaComponent implements OnInit {

  displayedColumns = [
    'year',
    'type',
    'rest'
  ];

  dataSource;

  ELEMENT_DATA;

  @ViewChild(MatSort) sort: MatSort;

  selection;


  displayedColumns2 = [
    'year',
    'type',
    'amount',
    'aux'
  ];

  dataSource2;

  ELEMENT_DATA2;

  @ViewChild(MatSort) sort2: MatSort;

  selection2;
  activeExport: string;

  isLoadingResults: boolean;

  page: number;
  totalpages: number;
  paginator: Page[];

  disablePrev: string;
  disableNext: string;

  showLoading: string;
  showTableData: string;

  cashClosed: CashClosed[];
  cashNotClosed: CashNotClosed[];



  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private http: HttpClient,
    private modalService: ModalDialogService,
    private viewRef: ViewContainerRef,
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig
    ) {
      this.toastyConfig.theme = 'bootstrap';
    }

  ngOnInit() {

    this.isLoadingResults = true;

    /*this.route.params.subscribe(params => {
      this.page = +params['p'];
    });*/

    this.page = 1;

    if (this.page == 1) {
      this.disablePrev = 'disabled';
      this.disableNext = 'disabled';
    }
    if (this.page == this.totalpages) {
      this.disableNext = 'disabled';
    }
    if (this.page > 1) {
      this.disablePrev = '';
    }
    if (this.page < this.totalpages) {
      this.disableNext = '';
    }

    this.isLoadingResults = true;

    this.createTableRegisters();
  }

  CloseCash(cash) {
    const data = new FormData();
    data.append('type', cash.type);
    data.append('year', cash.year);
    data.append('rest', cash.amount);
    this.http.post(localStorage.getItem('baseurl') + 'list_cash_not_closed/', data)
      .subscribe(
        res => {
          this.toastyService.success('Se Cerro la Caja Exitosamente');
          this.isLoadingResults = true;
          this.createTableRegisters();
        },
        error => {
          console.log(error.error.text);
        }
      );
  }

  stripDate(str: string) {
    return str.split('T')[0];
  }

  export() {

  }

  selectChange(e: any, row: any) {
    e ? this.selection.toggle(row) : null;


    if(this.selection.hasValue()){
      this.activeExport = '';
    } else {
      this.activeExport = 'disabled';
    }
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  parseType(x) {
    let list_types = [
      {id: '1', name: 'Caja Soles'},
      {id: '2', name: 'Caja Dólares'},
      {id: '3', name: 'Banco M/N Cta Cte'},
      {id: '4', name: 'Banco M/E Cta Cte'},
    ];
    for(let xx of list_types) {
      if(xx.id == x) {
        return xx.name;
      }
    }
  }

  createTableRegisters() {
    this.http.get(localStorage.getItem('baseurl') + 'list_cash_closed/?page=' + this.page)
      .subscribe(
        res => {
          this.cashClosed = JSON.parse(JSON.stringify(res)).objects;
          this.isLoadingResults = false;
          this.dataSource = new MatTableDataSource<CashClosed>(this.cashClosed);
          this.selection = new SelectionModel<CashClosed>(true, []);
          this.dataSource.sort = this.sort;
        },
        error => {
          console.log(error.error.text);
        }
      );

    this.http.get(localStorage.getItem('baseurl') + 'list_cash_not_closed/')
      .subscribe(
        res => {
          this.cashNotClosed = JSON.parse(JSON.stringify(res)).objs;
          console.log(res);
          this.isLoadingResults = false;
          this.dataSource2 = new MatTableDataSource<CashNotClosed>(this.cashNotClosed);
          this.selection2 = new SelectionModel<CashNotClosed>(true, []);
          this.dataSource2.sort = this.sort2;
        },
        error => {
          console.log(error.error.text);
        }
      );
  }

  changePage(p) {
    this.isLoadingResults = true;
    this.showLoading = 'show-loading';
    this.showTableData = 'hide-data';
    for(let i of this.paginator) {
      i.state = '';
      if (i.page == p) {
        i.state = 'active';
      }
    }
    this.page = p;
    if(this.page == 1) {
      this.disablePrev = 'disabled';
      this.disableNext = 'disabled';
    }
    if(this.page == this.totalpages) {
      this.disableNext = 'disabled';
    }
    if(this.page > 1) {
      this.disablePrev = '';
    }
    if(this.page < this.totalpages) {
      this.disableNext = '';
    }

    this.location.replaceState("/registro/"+this.page);

    this.http.get(localStorage.getItem('baseurl') + 'list_documents?page='+this.page)
      .subscribe(
        res => {
          let aux = JSON.stringify(res);

          this.cashClosed = JSON.parse(aux).objects;

          this.createTableRegisters();
          this.showLoading = 'hide-loading';
          this.showTableData = 'show-data';
        },
        error => {
          console.log(error.error.text);
        }
      );
  }

  previusPage() {
    this.changePage(this.page - 1);
  }

  nextPage() {
    this.changePage(this.page + 1);
  }

}
