import { Routes } from '@angular/router';

import { RegistroComponent } from './registro.component';
import { RegistroBComponent } from './registrob/registrob.component';
import { AddRegistroComponent } from './addregistro/addregistro.component';
import { EditRegistroComponent } from './editregistro/editregistro.component';
import { ViewRegistroComponent } from './viewregistro/viewregistro.component';
import { FilterregistroComponent } from './filter/filterregistro.component';
import { CanDeactivateGuard } from '../preventexit/prevent';

export const RegistroRoutes: Routes = [{
  path: '',
  component: RegistroComponent,
  data: {
    breadcrumb: 'Registro',
    status: true
  },
  children: [
      {
        path: ':p',
          component: RegistroBComponent,
          data: {
              breadcrumb: 'Documentos',
              status: false
          }
      },
      {
        path: 'add/new',
          component: AddRegistroComponent,
          data: {
              breadcrumb: 'Añadir Documento',
              status: false
          },
          canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'add/new/:trip',
          component: AddRegistroComponent,
          data: {
              breadcrumb: 'Añadir Documento',
              status: false
          },
          canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'edit/:id',
        component: EditRegistroComponent,
        data: {
          breadcrumb: 'Editar Documento',
          status: false
        },
        canDeactivate: [CanDeactivateGuard]
      },
      {
        path: 'view/:id',
        component: ViewRegistroComponent,
        data: {
          breadcrumb: 'Ver Documento',
          status: false
        }
      },
      {
        path: 'filter/:type/:param1/:param2/:page',
        component: FilterregistroComponent,
        data: {
          breadcrumb: 'Filtrar Documento',
          status: false
        }
      },
      {
        path: 'filter/:type/:param1/:page',
        component: FilterregistroComponent,
        data: {
          breadcrumb: 'Filtrar Documento',
          status: false
        }
      }
    ]
}];
