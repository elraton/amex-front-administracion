import {Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';

@Component({
  selector: 'app-utilidades',
  templateUrl: './editutilidades.component.html',
  styleUrls: ['./editutilidades.component.css']
})

export class EditUtilidadesComponent implements OnInit {
	name: string;
	type: string;
	util_name: string;
	id: number;

	operation_types_list: any[];
	trip_types_list: any[];
	category_list: any[];
	document_type_list: any[];
	
	constructor(
		private router: Router,
		private location: Location,
		private route: ActivatedRoute,
		private http: HttpClient,
		private toastyService: ToastyService,
		private toastyConfig: ToastyConfig
	) {
		this.toastyConfig.theme = 'bootstrap';
	}

	ngOnInit() {
		this.name = '';
		this.route.params.subscribe(params => {
			this.type = params['t'];
			this.id = +params['id'];
	    });

	    this.http.get(localStorage.getItem('baseurl') + 'list_utilities/')
	      .subscribe(
	        response => {
	          let aux = JSON.stringify(response);
	          this.operation_types_list = JSON.parse(aux).operation_types;
	          this.trip_types_list = JSON.parse(aux).trip_types;
			  this.category_list = JSON.parse(aux).categories;
			  this.document_type_list = JSON.parse(aux).document_types;

			  	if(this.type == '1') {
			    	this.util_name = 'Tipo de viaje';
			    	for(let xx of this.trip_types_list) {
			    		if(xx.id === this.id) {
			    			this.name = xx.name;
			    		}
			    	}
			    }
			    if(this.type == '2') {
			    	this.util_name = 'Categoria';
			    	for(let xx of this.category_list) {
			    		if(xx.id === this.id) {
			    			this.name = xx.name;
			    		}
			    	}
			    }
			    if(this.type == '3') {
			    	this.util_name = 'Tipo de operación';
			    	for(let xx of this.operation_types_list) {
			    		if(xx.id === this.id) {
			    			this.name = xx.name;
			    		}
			    	}
			    }
			    if(this.type == '4') {
			    	this.util_name = 'Tipo de documento';
			    	for(let xx of this.document_type_list) {
			    		if(xx.id === this.id) {
			    			this.name = xx.name;
			    		}
			    	}
			    }
	        },
	        error => {
	          console.log(error);
	        }
	      );

	}

	save() {
		if(this.name != '') {
			let data = {
				'type': this.type,
				'name': this.name,
				'id': this.id
			};

			this.http.post(localStorage.getItem('baseurl') + 'edit_utilities/', data)
		      .subscribe(
		        res => {
		          console.log(res);
		          this.location.back();
		        },
		        error => {
		          console.log(error);

		          if(
		          	error.error.text === 'Trip type updated' ||
		          	error.error.text === 'Category updated' ||
		          	error.error.text === 'Operation type updated' ||
		          	error.error.text === 'Document type updated'
		          	) {
									this.toastyService.success('Registro Editado');
									setTimeout(() => {
										this.router.navigate(['/utilidades']);
									}, 1000);
		          }
		        }
		      );
		} else {
			this.toastyService.warning('Debe ingresar un nombre');
		}
	}
  cancelar() {
    this.location.back();
  }
}
