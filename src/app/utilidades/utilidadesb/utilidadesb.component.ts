import {Component, OnInit, ViewContainerRef} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ModalDialogService, SimpleModalComponent } from 'ngx-modal-dialog';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';

@Component({
  selector: 'app-utilidades',
  templateUrl: './utilidadesb.component.html',
  styleUrls: ['./utilidadesb.component.css']
})

export class UtilidadesBComponent implements OnInit {

	operation_types_list: any[];
	trip_types_list: any[];
	category_list: any[];
	document_type_list: any[];

	operation_type: string;
	trip_type: string;
	category: string;
	document_type: string;

	constructor(
		private http: HttpClient,
		private router: Router,
		private modalService: ModalDialogService,
			private viewRef: ViewContainerRef,
			private toastyService: ToastyService,
		private toastyConfig: ToastyConfig
		) {
			this.toastyConfig.theme = 'bootstrap';
		 }
	
	ngOnInit() {
		
	    this.http.get(localStorage.getItem('baseurl') + 'list_utilities/')
		      .subscribe(
		        response => {
		          console.log(response);
		          let aux = JSON.stringify(response);
		          this.operation_types_list = JSON.parse(aux).operation_types;
		          this.trip_types_list = JSON.parse(aux).trip_types;
				  this.category_list = JSON.parse(aux).categories;
				  this.document_type_list = JSON.parse(aux).document_types;
		        },
		        error => {
		          console.log(error);
		        }
		      );

		this.operation_type = '0';
		this.trip_type = '0';
		this.category = '0';
		this.document_type = '0';
	}

	delete(t:number) {

		let id2delete = 0;

		switch (t) {
			case 1:{
				if(this.trip_type != '0'){
					id2delete = +this.trip_type;
				}
				break;
			}
			case 2:{
				if(this.category != '0'){
					id2delete = +this.category;
				}
				break;
			}
			case 3:{
				if(this.operation_type != '0'){
					id2delete = +this.operation_type;
				}
				break;
			}
			case 4:{
				if(this.document_type != '0'){
					id2delete = +this.document_type;
				}
				break;
			}
			default:
				// code...
				break;
		}

		if(id2delete != 0) {
			this.modalService.openDialog(this.viewRef, {
		      title: '¿Seguro que quieres eliminar esta Utilidad?',
		      childComponent: SimpleModalComponent,
		      data: {
		        text: ''
		      },
		      settings: {
		        closeButtonClass: 'close theme-icon-close'
		      },
		      actionButtons: [
		        {
		          text: 'Si',
		          buttonClass: 'btn btn-danger',
		          onAction: () => new Promise((resolve: any, reject: any) => {
		            setTimeout(() => {
		              const varData = {
		              	'type': t.toString(),
		              	'id': id2delete
		              };
		              
		              this.http.post(localStorage.getItem('baseurl') + 'delete_utilities/', varData)
		                .subscribe(
		                  res => {
		                    console.log(res);
		                    //this.location.back();
		                  },
		                  error => {
		                    console.log(error.error.text);
		                    if(
		                    	error.error.text === 'Trip type deleted' ||
		                    	error.error.text === 'Category deleted' ||
		                    	error.error.text === 'Operation type deleted' ||
		                    	error.error.text === 'Document type deleted'
		                    	) {
														this.toastyService.success('Registro Eliminado');

		                    	this.http.get(localStorage.getItem('baseurl') + 'list_utilities/')
							      .subscribe(
							        response => {
							          console.log(response);
							          let aux = JSON.stringify(response);
							          this.operation_types_list = JSON.parse(aux).operation_types;
							          this.trip_types_list = JSON.parse(aux).trip_types;
									  this.category_list = JSON.parse(aux).categories;
									  this.document_type_list = JSON.parse(aux).document_types;
							        },
							        error => {
							          console.log(error);
							        }
							      );

		                      resolve();
		                    }
		                  }
		                );
		            }, 20);
		          })
		        },
		        {
		          text: 'No',
		          buttonClass: 'btn btn-success',
		          onAction: () => new Promise((resolve: any) => {
		            setTimeout(() => {
		              resolve();
		            }, 20);
		          })
		        },
		      ]
		    });
		}


		
	}

	editar(t:number) {
		switch (t) {
			case 1:{
				if(this.trip_type != '0'){
					this.router.navigate(['/utilidades/edit/' + t + '/'+this.trip_type]);
				}
				break;
			}
			case 2:{
				if(this.category != '0'){
					this.router.navigate(['/utilidades/edit/' + t + '/'+this.category]);
				}
				break;
			}
			case 3:{
				if(this.operation_type != '0'){
					this.router.navigate(['/utilidades/edit/' + t + '/'+this.operation_type]);
				}
				break;
			}
			case 4:{
				if(this.document_type != '0'){
					this.router.navigate(['/utilidades/edit/' + t + '/'+this.document_type]);
				}
				break;
			}
			default:
				// code...
				break;
		}
	}
}
