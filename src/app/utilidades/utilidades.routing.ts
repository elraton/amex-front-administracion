import { Routes } from '@angular/router';

import { UtilidadesComponent } from './utilidades.component';
import { UtilidadesBComponent } from './utilidadesb/utilidadesb.component';
import { AddUtilidadesComponent } from './addutilidades/addutilidades.component';
import { EditUtilidadesComponent } from './editutilidades/editutilidades.component';

export const UtilidadesRoutes: Routes = [{
	path: '',
	component: UtilidadesComponent,
	data: {
		breadcrumb: 'Utilidades',
		status: false
	},
	children: [
		{
			path: '',
			component: UtilidadesBComponent,
			data: {
				breadcrumb: 'Utilidades',
				status: false
			}
		},
		{
			path: 'add/:t',
			component: AddUtilidadesComponent,
			data: {
				breadcrumb: 'Añadir Utilidades',
				status: false
			}
		},
		{
			path: 'edit/:t/:id',
			component: EditUtilidadesComponent,
			data: {
				breadcrumb: 'Editar Utilidades',
				status: false
			}
		},
	]
}];
