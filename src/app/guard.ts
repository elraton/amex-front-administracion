import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

	constructor
	(
		public router: Router,
  	public http: HttpClient
  	) {

  }

  canActivate() {
    console.log('i am checking to see if you are logged in');
    let token = '';
		token = localStorage.getItem('token');
		console.log(token);
    if (token === '') {
			// no hay token redirige a login
			this.router.navigate(['/authentication/login']);
    } else {
    	// verificar si el token es valido
      return true;

    	// const formData = new FormData();

    	// formData.append("token", token);

    	/*let data = {'token': token};
    	const headers = new HttpHeaders().set('Content-Type', 'application/json');
		
    	this.http.post(localStorage.getItem('baseurl') + 'token_verify/', JSON.stringify(data), {headers: headers})
	      .subscribe(
	        res => {
						localStorage.setItem('token', JSON.parse(JSON.stringify(res)).token);
						return true;
	        },
	        error => {
	          console.log(error);
	          this.router.navigate(['/authentication/login']);
	        }
	      );*/

    }

    return true;
  }

  canActivateChild() {
    console.log('checking child route access');
		let token = '';
		token = localStorage.getItem('token');
		console.log(token === '');
    if (token === '') {
    	//no hay token redirige a login
    	this.router.navigate(['/authentication/login']);
    } else {
      return true;
    	// verificar si el token es valido  

    	/*let formData = new FormData();

    	// formData.append("token", token);

    	const data = {'token': token};
    	const headers = new HttpHeaders().set('Content-Type', 'application/json');
		
    	this.http.post(localStorage.getItem('baseurl') + 'token_verify/', JSON.stringify(data), {headers: headers})
	      .subscribe(
	        res => {
	          localStorage.setItem('token', JSON.parse(JSON.stringify(res)).token);
	        },
	        error => {
	          console.log(error);
	          this.router.navigate(['/authentication/login']);
	        }
	      );*/

    }
    return true;
  }

}